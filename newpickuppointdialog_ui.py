# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\newpickuppointdialog.ui'
#
# Created: Sat Apr 30 03:18:26 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_new_pickuppoint_dialog(object):
    def setupUi(self, new_pickuppoint_dialog):
        new_pickuppoint_dialog.setObjectName("new_pickuppoint_dialog")
        new_pickuppoint_dialog.resize(new_pickuppoint_dialog.sizeHint().width(), new_pickuppoint_dialog.sizeHint().height())
        new_pickuppoint_dialog.setModal(True)
        self.formLayout = QtGui.QFormLayout(new_pickuppoint_dialog)
        self.formLayout.setObjectName("formLayout")
        self.label_pickuppoint_name = QtGui.QLabel(new_pickuppoint_dialog)
        self.label_pickuppoint_name.setObjectName("label_pickuppoint_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_pickuppoint_name)
        self.lineedit_pickuppoint_name = QtGui.QLineEdit(new_pickuppoint_dialog)
        self.lineedit_pickuppoint_name.setObjectName("lineedit_pickuppoint_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineedit_pickuppoint_name)
        self.buttonbox_pickuppoint = QtGui.QDialogButtonBox(new_pickuppoint_dialog)
        self.buttonbox_pickuppoint.setOrientation(QtCore.Qt.Horizontal)
        self.buttonbox_pickuppoint.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonbox_pickuppoint.setObjectName("buttonbox_pickuppoint")
        self.formLayout.setWidget(3, QtGui.QFormLayout.SpanningRole, self.buttonbox_pickuppoint)

        self.retranslateUi(new_pickuppoint_dialog)
        QtCore.QObject.connect(self.buttonbox_pickuppoint, QtCore.SIGNAL("accepted()"), new_pickuppoint_dialog.accept)
        QtCore.QObject.connect(self.buttonbox_pickuppoint, QtCore.SIGNAL("rejected()"), new_pickuppoint_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(new_pickuppoint_dialog)

    def retranslateUi(self, new_pickuppoint_dialog):
        new_pickuppoint_dialog.setWindowTitle(QtGui.QApplication.translate("new_pickuppoint_dialog", "Add New Pickup Point", None, QtGui.QApplication.UnicodeUTF8))
        self.label_pickuppoint_name.setText(QtGui.QApplication.translate("new_pickuppoint_dialog", "Name:", None, QtGui.QApplication.UnicodeUTF8))

