from PySide.QtCore import QAbstractTableModel
import PySide.QtCore
import classes
import operator
import stringified_classes


class ObjectTable(QAbstractTableModel):
    def __init__(self, list_of_obj, headers, parent=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.objects = list_of_obj
        self.cols = headers
        #self.cols = list(self.objects[0].__dict__.keys())
        self.headers = list(i.title() for i in self.cols)

    def rowCount(self, parent):
        return len(self.objects)

    def columnCount(self, parent):
        return len(self.headers)

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role != PySide.QtCore.Qt.DisplayRole:
            return None
        res = getattr(self.objects[index.row()], self.cols[index.column()])
        return res

    def get_id(self, i=int):
        return self.objects[i].id

    def sort(self, col, order):
        self.layoutAboutToBeChanged.emit()
        self.objects = sorted(self.objects,
                                        key= lambda n: n.name)
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.objects.reverse()
        self.layoutChanged.emit()

    def headerData(self, col, orientation, role):
        if orientation == PySide.QtCore.Qt.Horizontal and role == PySide.QtCore.Qt.DisplayRole:
            return self.headers[col]
        return None
    # def sort(self, col, order):
    #     """sort table by given column number col"""
    #     self.layoutAboutToBeChanged.emit()
    #     self.objects = sorted(self.objects,
    #         key=operator.itemgetter(col))
    #     if order == Qt.DescendingOrder:
    #         self.mylist.reverse()
    #     self.layoutChanged.emit()


class SpecialTable(ObjectTable):
    def __init__(self, list_of_obj, ticket_util=False, parent=None, *args):
        objects = list_of_obj
        n = objects[0].__class__.__name__
        cols = []
        if n == "Bus":
            cols = ["name", "capacity"]
            self.sort = self.sort_buses
        elif n in ["Guide", "Driver"]:
            cols = ["name", "phone", "comments"]
            self.sort = self.sort_drivers_and_guides
        elif n == "Route":
            cols = ["name", "price"]
            self.sort = self.sort_routes
        elif n in ["Hotel", "Pickup_Point"]:
            cols = ["name"]
            self.sort = self.sort_hotels
        elif n == "Vendor":
            cols = ["name", "afm", "phone", "fax", "email"]
            self.sort = self.sort_vendors
        ObjectTable.__init__(self, list_of_obj=objects, headers=cols, parent=parent)

    def sort_vendors(self, col, order):
        self.layoutAboutToBeChanged.emit()
        if col == 0:
            self.objects = sorted(self.objects, key= lambda n: n.name)
        elif col == 1:
            self.objects = sorted(self.objects, key= lambda n: n.afm)
        elif col == 2:
            self.objects = sorted(self.objects, key= lambda n: n.phone)
        elif col == 3:
            self.objects = sorted(self.objects, key= lambda n: n.fax)
        else:
            self.objects = sorted(self.objects, key= lambda n: n.email)
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.objects.reverse()
        self.layoutChanged.emit()

    def sort_drivers_and_guides(self, col, order):
        self.layoutAboutToBeChanged.emit()
        if col == 0:
            self.objects = sorted(self.objects, key= lambda n: n.name)
        elif col == 1:
            self.objects = sorted(self.objects, key= lambda n: n.phone)
        else:
            self.objects = sorted(self.objects, key= lambda n: n.comments)
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.objects.reverse()
        self.layoutChanged.emit()
    def sort_buses(self, col, order):
        self.layoutAboutToBeChanged.emit()
        if col == 0:
            self.objects = sorted(self.objects, key= lambda n: n.name)
        else:
            self.objects = sorted(self.objects, key= lambda n: n.capacity)
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.objects.reverse()
        self.layoutChanged.emit()
    def sort_routes(self, col, order):
        self.layoutAboutToBeChanged.emit()
        if col == 0:
            self.objects = sorted(self.objects, key= lambda n: n.name)
        else:
            self.objects = sorted(self.objects, key= lambda n: n.price)
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.objects.reverse()
        self.layoutChanged.emit()
    def sort_hotels(self, col, order):
        self.layoutAboutToBeChanged.emit()
        self.objects = sorted(self.objects,
                                        key= lambda n: n.name)
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.objects.reverse()
        self.layoutChanged.emit()


class RoutesTable(QAbstractTableModel):
    def __init__(self, routes_list, parent=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.routes_times_list = routes_list  # [0]= ID, [1] = NAME, [2] = Delta T, [3] =Start_Time
        self.headers = ["Name", "Time difference", "Result"]
    def rowCount(self, parent):
        return len(self.routes_times_list)

    def columnCount(self, parent):
        return 3

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role != PySide.QtCore.Qt.DisplayRole:
            return None
        if index.column() == 0:
            return self.routes_times_list[index.row()][1]
        elif index.column() == 1:
            return self.routes_times_list[index.row()][2]
        else:
            return self.routes_times_list[index.row()][3].addSecs(self.routes_times_list[index.row()][2]*60)

    def set_dt(self, row, dt):
        self.routes_times_list[row][2] = dt

    def get_id_t(self, index):
        if index.row() >= len(self.routes_times_list):
            return None, None
        return self.routes_times_list[index.row()][0], self.routes_times_list[index.row()][2]

    def sort(self, col, order):
        self.layoutAboutToBeChanged.emit()
        self.routes_times_list = sorted(self.routes_times_list,
                                        key=operator.itemgetter(col+1))
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.routes_times_list.reverse()
        self.layoutChanged.emit()



    def headerData(self, col, orientation, role):
        if orientation == PySide.QtCore.Qt.Horizontal and role == PySide.QtCore.Qt.DisplayRole:
            return self.headers[col]
        return None


class PickupsTable(QAbstractTableModel):
    def __init__(self, pickup_list, start_time,  parent=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.pickup_list = pickup_list
        self.headers = ["Name", "Time difference", "Result"]
        self.start_time = start_time
    def new_time(self, time):
        self.start_time = time

    def rowCount(self, parent):
        return len(self.pickup_list)

    def columnCount(self, parent):
        return 3

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role != PySide.QtCore.Qt.DisplayRole:
            return None
        if index.column() == 0:
            return self.pickup_list[index.row()][1]
        elif index.column() == 1:
            return self.pickup_list[index.row()][2]
        else:
            #print(self.pickup_list[index.row()][2])
            return self.start_time.addSecs(self.pickup_list[index.row()][2]*60)

    def set_dt(self, row, dt):
        self.pickup_list[row][2] = dt
        #print(self.pickup_list)

    def get_id_t(self, row):
        if row >= len(self.pickup_list):
            return None, None
        return self.pickup_list[row][0], self.pickup_list[row][2]

    def sort(self, col, order):
        self.layoutAboutToBeChanged.emit()
        if col == 2:
            self.pickup_list = sorted(self.pickup_list,
                                      key=lambda n: self.start_time.addSecs(n[2]*60))
        else:
            self.pickup_list = sorted(self.pickup_list,
                                        key=operator.itemgetter(col+1))
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.pickup_list.reverse()
        self.layoutChanged.emit()

    def headerData(self, col, orientation, role):
        if orientation == PySide.QtCore.Qt.Horizontal and role == PySide.QtCore.Qt.DisplayRole:
            return self.headers[col]
        return None


class Util_Tickets(QAbstractTableModel):
    def __init__(self, ticketlist):
        QAbstractTableModel.__init__(self)
        self.datalist = ticketlist
        self.headers = ["Time", "Pickup", "Hotel", "Room", "Name", "Voucher", "Adults", "Children", "Infants",
                        "Lang", "PoB",
                        "Rep", "Comments"]


    def rowCount(self, parent):
        return len(self.datalist)

    def columnCount(self, parent):
        return len(self.headers)

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role != PySide.QtCore.Qt.DisplayRole:
            return None
        return self.datalist[index.row()][index.column()+1]
    #
    # def get_id(self, i=int):
    #     if i < 0 or i >= len(self.datalist):
    #         return None
    #     return self.datalist[i][0]

    def get_id(self, index):
        if not index.isValid():
            return None
        return self.datalist[index.row()][0]

    def sort(self, col, order):
        self.layoutAboutToBeChanged.emit()
        self.datalist = sorted(self.datalist,
                                        key=operator.itemgetter(col+1))
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.datalist.reverse()
        self.layoutChanged.emit()

    def headerData(self, col, orientation, role):
        if orientation == PySide.QtCore.Qt.Horizontal and role == PySide.QtCore.Qt.DisplayRole:
            return self.headers[col]
        return None


class Fin_Tickets(QAbstractTableModel):
    def __init__(self, ticketlist):
        QAbstractTableModel.__init__(self)
        self.datalist = ticketlist
        self.headers = ["Rep", "Date", "Route", "Voucher", "Language", "AD", "CH", "IN", "Price", "PoB"]


    def rowCount(self, parent):
        return len(self.datalist)

    def columnCount(self, parent):
        return 10

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role != PySide.QtCore.Qt.DisplayRole:
            return None
        return self.datalist[index.row()][index.column()+1]
    #
    # def get_id(self, i=int):
    #     if i < 0 or i >= len(self.datalist):
    #         return None
    #     return self.datalist[i][0]

    def get_id(self, index):
        if not index.isValid():
            return None
        return self.datalist[index.row()][0]

    def sort(self, col, order):
        self.layoutAboutToBeChanged.emit()
        self.datalist = sorted(self.datalist,
                                        key=operator.itemgetter(col+1))
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.datalist.reverse()
        self.layoutChanged.emit()

    def headerData(self, col, orientation, role):
        if orientation == PySide.QtCore.Qt.Horizontal and role == PySide.QtCore.Qt.DisplayRole:
            return self.headers[col]
        return None


class Trip_Table(QAbstractTableModel):
    def __init__(self, trip_list,  parent=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.trip_list = trip_list
        self.headers = ["Name", "Date", "Bus", "Driver", "Guide", "Clients"]

    def rowCount(self, parent):
        return len(self.trip_list)

    def columnCount(self, parent):
        return 6

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role != PySide.QtCore.Qt.DisplayRole:
            return None
        return self.trip_list[index.row()][index.column()+1]

    def get_id(self, index):
        if not index.isValid():
            return None
        return self.trip_list[index.row()][0]

    def sort(self, col, order):
        self.layoutAboutToBeChanged.emit()
        self.trip_list = sorted(self.trip_list,
                                        key=operator.itemgetter(col+1))
        if order == PySide.QtCore.Qt.DescendingOrder:
            self.trip_list.reverse()
        self.layoutChanged.emit()

    def headerData(self, col, orientation, role):
        if orientation == PySide.QtCore.Qt.Horizontal and role == PySide.QtCore.Qt.DisplayRole:
            return self.headers[col]
        return None