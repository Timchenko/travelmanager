from PySide import QtGui
import sys


app = QtGui.QApplication(sys.argv)
firs = QtGui.QDialog()
a = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
firs.formLayout = QtGui.QFormLayout(firs)
firs.formLayout.setWidget(1, QtGui.QFormLayout.SpanningRole, a)
firs.setModal(True)
firs.show()

secon = QtGui.QDialog()
comb = QtGui.QComboBox()
a = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
secon.formLayout = QtGui.QFormLayout(secon)
secon.formLayout.setWidget(0, QtGui.QFormLayout.SpanningRole, comb)
secon.formLayout.setWidget(1, QtGui.QFormLayout.SpanningRole, a)
secon.exec_()