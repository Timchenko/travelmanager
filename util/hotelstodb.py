import classes
import json

engine = classes.init_db()
intended error
data = None
with open('reordered.txt', "r") as f:
    data = json.load(f)
    #data = f.read()
    #data = json.loads(data)
    #f.close()

if data is None:
    raise Exception

sections = None

with open('pickups.txt', "r") as f:
    sections = f.read()
    sections = json.loads(sections)
    f.close()

if sections is None:
    raise Exception

d = {}
for i in range(len(sections)):
    d.update({sections[i]: data[i]})

# with open("theend.txt", "w") as f:
#     s = json.dumps(d, sort_keys = True, indent = 4, ensure_ascii=False, separators=(',', ': '))
#     f.write(s)
#     f.close()

session = classes.create_session(engine)

for i in d:
    name = i
    names_list = []
    hotels = []
    for j in d[i]:
        names_list.append(j)
        for k in d[i][j]:
            hotels.append(classes.Hotel(name=k))
    n = json.dumps(names_list)
    pu = classes.Pickup_Point(name=name, list_names=n)
    session.add(pu)
    for j in hotels:
        j.pickup_points = pu
        session.add(j)

classes.kill_session(session)