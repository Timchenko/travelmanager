import classes

eng = classes.init_db()
s = classes.create_session(eng)
times = s.query(classes.Pickup_Time).filter_by(routes_id=4).all()
for i in times:
    nt = classes.Pickup_Time(time=i.time,
                             pickup_points_id=i.pickup_points_id,
                             routes_id=9)
    s.add(nt)
classes.kill_session(s)