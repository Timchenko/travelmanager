import json

def split_hotels(stri):
    u = stri.split("-")
    result = []
    for i in u:
        n = i.strip()
        result.append(n)
    return result
def split_pickups(stri):
    result = {}
    while True:
        i = stri.find('(')
        if i == -1:
            break
        strt = stri[0:i]
        key = stri[i+1:stri.find(')')]
        stri = stri[stri.find(')')+1: len(stri)]
        result.update({key: strt})
    return result

def split_sections(stri):
    return stri.split('==========')


Wwith open('pickups.txt', 'r') as f:
    u = f.read()
    m = split_sections(u)
    rez = []
    for i in m:
        d = split_pickups(i)
        r = {}
        for j in d:
            u = split_hotels(d[j])
            r.update({j: u})
        rez.append(r)

    with open(r'./result.txt', 'w') as w:
        w.write(json.dumps(rez, sort_keys=True, indent=4, ensure_ascii=False, separators=(',', ': ')))