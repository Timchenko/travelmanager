# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\newguidedialog.ui'
#
# Created: Sat Apr 30 03:18:37 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_new_guide_dialog(object):
    def setupUi(self, new_guide_dialog):
        new_guide_dialog.setObjectName("new_guide_dialog")
        new_guide_dialog.resize(new_guide_dialog.sizeHint().width(), new_guide_dialog.sizeHint().height())
        new_guide_dialog.setModal(True)
        self.formLayout = QtGui.QFormLayout(new_guide_dialog)
        self.formLayout.setObjectName("formLayout")
        self.label_guide_name = QtGui.QLabel(new_guide_dialog)
        self.label_guide_name.setObjectName("label_guide_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_guide_name)
        self.lineedit_guide_name = QtGui.QLineEdit(new_guide_dialog)
        self.lineedit_guide_name.setObjectName("lineedit_guide_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineedit_guide_name)
        self.label_guide_phone = QtGui.QLabel(new_guide_dialog)
        self.label_guide_phone.setObjectName("label_guide_phone")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_guide_phone)
        self.lineedit_guide_phone = QtGui.QLineEdit(new_guide_dialog)
        self.lineedit_guide_phone.setObjectName("lineedit_guide_phone")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineedit_guide_phone)
        self.label_guide_comments = QtGui.QLabel(new_guide_dialog)
        self.label_guide_comments.setObjectName("label_guide_comments")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_guide_comments)
        self.lineedit_guide_comments = QtGui.QLineEdit(new_guide_dialog)
        self.lineedit_guide_comments.setObjectName("lineedit_guide_comments")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineedit_guide_comments)
        self.buttonbox_guide = QtGui.QDialogButtonBox(new_guide_dialog)
        self.buttonbox_guide.setOrientation(QtCore.Qt.Horizontal)
        self.buttonbox_guide.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonbox_guide.setObjectName("buttonbox_guide")
        self.formLayout.setWidget(3, QtGui.QFormLayout.SpanningRole, self.buttonbox_guide)

        self.retranslateUi(new_guide_dialog)
        QtCore.QObject.connect(self.buttonbox_guide, QtCore.SIGNAL("accepted()"), new_guide_dialog.accept)
        QtCore.QObject.connect(self.buttonbox_guide, QtCore.SIGNAL("rejected()"), new_guide_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(new_guide_dialog)

    def retranslateUi(self, new_guide_dialog):
        new_guide_dialog.setWindowTitle(QtGui.QApplication.translate("new_guide_dialog", "Add Guide", None, QtGui.QApplication.UnicodeUTF8))
        self.label_guide_name.setText(QtGui.QApplication.translate("new_guide_dialog", "Name:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_guide_phone.setText(QtGui.QApplication.translate("new_guide_dialog", "Phone:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_guide_comments.setText(QtGui.QApplication.translate("new_guide_dialog", "Comments:", None, QtGui.QApplication.UnicodeUTF8))

