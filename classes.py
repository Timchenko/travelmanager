__author__ = 'Kucher'

from sqlalchemy import ForeignKey, create_engine, Column, Integer, Unicode, Float, DateTime, Time
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
import json, datetime


Base = declarative_base()

class Route(Base):
    __tablename__ = 'routes'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(50))
    price = Column(Float)
    start_time = Column(Time)
    pickup_points = relationship('Pickup_Time', back_populates = 'routes')

    def __repr__(self):
        return "<Route(name = '%s', price = '%f')>" %(self.name, self.price)


class Bus(Base):
    __tablename__ = 'buses'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(50))
    capacity = Column(Integer)

    def __repr__(self):
        return "<Bus(name = '%s')>" %(self.name)


class Guide(Base):
    __tablename__ = 'guides'
    id =  Column(Integer, primary_key=True)
    name = Column(Unicode(50))
    phone = Column(Unicode(20))
    comments = Column(Unicode(200))

    def __repr__(self):
        return "<Guide(name = '%s', phone = '%s')>" %(self.name, self.phone)


class Driver(Base):
    __tablename__ = 'drivers'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(50))
    phone = Column(Unicode(50))
    comments = Column(Unicode(200))

    def __repr__(self):
        return "<Driver(name = '%s', phone = '%s')>" %(self.name, self.phone)


class Pickup_Point(Base):
    __tablename__ = 'pickup_points'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(100))
    list_names = Column(Unicode(1500))
    routes = relationship('Pickup_Time', back_populates = 'pickup_points')



    def __repr__(self):
        return "<Pickup_Point(name = '%s')>" %(self.name)


class Hotel(Base):
    __tablename__ = 'hotels'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(100))
    comments = Column(Unicode(100))
    pickup_points_id = Column(Integer, ForeignKey('pickup_points.id'))
    pickup_points = relationship('Pickup_Point', back_populates = 'hotels')

    def __repr__(self):
        return "<Hotel(name = '%s')>" %(self.name)


Pickup_Point.hotels = relationship('Hotel', order_by=Hotel.id, back_populates='pickup_points')


class Trip(Base):
    __tablename__ = 'trips'
    id = Column(Integer, primary_key=True)
    start_date_time = Column(DateTime)
    max_passengers = Column(Integer)
    routes_id = Column(Integer, ForeignKey('routes.id'))
    buses_id = Column(Integer, ForeignKey('buses.id'))
    drivers_id = Column(Integer, ForeignKey('drivers.id'))
    guides_id = Column(Integer, ForeignKey('guides.id'))

    routes = relationship('Route', back_populates = 'trips')
    buses = relationship('Bus', back_populates = 'trips')
    drivers = relationship('Driver', back_populates = 'trips')
    guides = relationship('Guide', back_populates = 'trips')

    def __repr__(self):
        return "<Trip(max_passengers = '%s', trip_date = '%s')>" %(self.max_passengers, self.start_date_time)


Route.trips = relationship('Trip', order_by=Trip.id, back_populates='routes')
Bus.trips = relationship('Trip', order_by=Trip.id, back_populates='buses')
Driver.trips = relationship('Trip', order_by=Trip.id, back_populates='drivers')
Guide.trips = relationship('Trip', order_by=Trip.id, back_populates='guides')


#association table для pickup_points <--> routes
class Pickup_Time(Base):
    __tablename__ = 'pickup_times'
    time = Column(Integer)
    pickup_points_id = Column(Integer, ForeignKey('pickup_points.id'), primary_key=True)
    routes_id = Column(Integer, ForeignKey('routes.id'), primary_key=True)
    pickup_points = relationship('Pickup_Point', back_populates = 'routes')
    routes = relationship('Route', back_populates = 'pickup_points')


    def __repr__(self):
        return "<Pickup_time(time = '%s')>" %(self.time)


# class Customer(Base):
#    __tablename__ = 'customers'
#    id = Column(Integer, primary_key=True)
# name = Column(Unicode(50))
#    phone = Column(Unicode(20))
#    email = Column(Unicode(50))
#    room = Column(Unicode(10))
#    hotels_id = Column(Integer, ForeignKey('hotels.id'))
#
#    hotels = relationship('Hotel', order_by = Hotel.id, back_populates = 'customers')
#
#    def __repr__(self):
# return "<Customer(name = %s, phone = %s, email = %s, room = %s)>" %(self.name, self.phone, self.email, self.room)

# Hotel.customers = relationship('Customer', order_by = Customer.id, back_populates = 'hotels')

class Language(Base):
    __tablename__ = 'languages'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(50))

    def __repr__(self):
        return "<Language(language = %s)>" %(self.name)

class Vendor(Base):
    __tablename__ = 'vendors'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(50))
    afm = Column(Unicode(50))
    phone = Column(Unicode(50))
    fax = Column(Unicode(50))
    email = Column(Unicode(50))

    def __repr__(self):
        return "<Vendor(name = '%s')>" %(self.name)


class Ticket(Base):
    __tablename__ = 'tickets'
    id = Column(Integer, primary_key=True)
    number = Column(Unicode(10))
    customer = Column(Unicode(100))
    adults = Column(Integer)
    children = Column(Integer)
    infants = Column(Integer)
    pay_on_bus = Column(Float, default=0)
    price = Column(Float)
    room = Column(Unicode(10))
    pickup_point_name = Column(Unicode(200))
    comments = Column(Unicode(500))
    date = Column(DateTime, default=datetime.datetime.now())
    hotels_id = Column(Integer, ForeignKey('hotels.id'))
    pickup_points_id = Column(Integer, ForeignKey('pickup_points.id'))
    trips_id = Column(Integer, ForeignKey('trips.id'))
    vendors_id = Column(Integer, ForeignKey('vendors.id'))
    languages_id = Column(Integer, ForeignKey('languages.id'))

    pickup_points = relationship('Pickup_Point', back_populates='tickets')
    hotels = relationship('Hotel', back_populates='tickets')
    trips = relationship('Trip', back_populates='tickets')
    vendors = relationship('Vendor', back_populates='tickets')
    languages = relationship('Language', back_populates='tickets')


    def __repr__(self):
        return "<Ticket(id = '%d')>" %(self.id)


Hotel.tickets = relationship('Ticket', order_by=Ticket.id, back_populates='hotels')
Trip.tickets = relationship('Ticket', order_by=Ticket.id, back_populates='trips')
Vendor.tickets = relationship('Ticket', order_by=Ticket.id, back_populates='vendors')
Language.tickets = relationship('Ticket', order_by=Ticket.id, back_populates='languages')
Pickup_Point.tickets = relationship('Ticket', order_by=Ticket.id, back_populates='pickup_points')

def init_db():
    with open('db_credentials.txt') as db_cred_file:
        db_credentials = json.load(db_cred_file)

    # engine = create_engine('sqlite:///testing.sqlite')
    # engine = create_engine('mysql+cymysql://root:f3P7Fi2lE572q7v@localhost/24ceaomh1xuvgio')
    engine = create_engine("mysql+mysqlconnector://%s:%s@%s/%s" % (db_credentials['db_user'], db_credentials['db_pass'],
                                                                   db_credentials['db_host'],
                                                                   db_credentials['db_name']))
    Base.metadata.create_all(engine)
    return engine


def create_session(engine):
    Session = sessionmaker(bind=engine)
    session = Session()
    return session


def kill_session(session):
    session.commit()
    session.close()


def save_object(what_to_save):
    session = create_session()
    session.add(what_to_save)
    kill_session(session)


def delete_object(what_to_delete):
    session = create_session()
    session.delete(what_to_delete)
    kill_session(session)
