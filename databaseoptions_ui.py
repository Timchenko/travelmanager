# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\databaseoptions.ui'
#
# Created: Tue Apr  5 06:26:16 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_dbOptionsDialog(object):
    def setupUi(self, dbOptionsDialog):
        dbOptionsDialog.setObjectName("dbOptionsDialog")
        dbOptionsDialog.resize(400, 145)
        self.formLayout = QtGui.QFormLayout(dbOptionsDialog)
        self.formLayout.setObjectName("formLayout")
        self.dbHostLabel = QtGui.QLabel(dbOptionsDialog)
        self.dbHostLabel.setObjectName("dbHostLabel")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.dbHostLabel)
        self.dbHostLineEdit = QtGui.QLineEdit(dbOptionsDialog)
        self.dbHostLineEdit.setObjectName("dbHostLineEdit")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.dbHostLineEdit)
        self.dbUserLabel = QtGui.QLabel(dbOptionsDialog)
        self.dbUserLabel.setObjectName("dbUserLabel")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.dbUserLabel)
        self.dbUserLineEdit = QtGui.QLineEdit(dbOptionsDialog)
        self.dbUserLineEdit.setObjectName("dbUserLineEdit")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.dbUserLineEdit)
        self.dbPWLabel = QtGui.QLabel(dbOptionsDialog)
        self.dbPWLabel.setObjectName("dbPWLabel")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.dbPWLabel)
        self.dbPWLineEdit = QtGui.QLineEdit(dbOptionsDialog)
        self.dbPWLineEdit.setObjectName("dbPWLineEdit")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.dbPWLineEdit)
        self.dbNameLabel = QtGui.QLabel(dbOptionsDialog)
        self.dbNameLabel.setObjectName("dbNameLabel")
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.dbNameLabel)
        self.dbNameLineEdit = QtGui.QLineEdit(dbOptionsDialog)
        self.dbNameLineEdit.setObjectName("dbNameLineEdit")
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.dbNameLineEdit)
        self.dbOptionsButtonBox = QtGui.QDialogButtonBox(dbOptionsDialog)
        self.dbOptionsButtonBox.setOrientation(QtCore.Qt.Horizontal)
        self.dbOptionsButtonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.dbOptionsButtonBox.setObjectName("dbOptionsButtonBox")
        self.formLayout.setWidget(4, QtGui.QFormLayout.SpanningRole, self.dbOptionsButtonBox)

        self.retranslateUi(dbOptionsDialog)
        QtCore.QObject.connect(self.dbOptionsButtonBox, QtCore.SIGNAL("accepted()"), dbOptionsDialog.accept)
        QtCore.QObject.connect(self.dbOptionsButtonBox, QtCore.SIGNAL("rejected()"), dbOptionsDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(dbOptionsDialog)

    def retranslateUi(self, dbOptionsDialog):
        dbOptionsDialog.setWindowTitle(QtGui.QApplication.translate("dbOptionsDialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.dbHostLabel.setText(QtGui.QApplication.translate("dbOptionsDialog", "Database host:", None, QtGui.QApplication.UnicodeUTF8))
        self.dbUserLabel.setText(QtGui.QApplication.translate("dbOptionsDialog", "Database user:", None, QtGui.QApplication.UnicodeUTF8))
        self.dbPWLabel.setText(QtGui.QApplication.translate("dbOptionsDialog", "Database password:", None, QtGui.QApplication.UnicodeUTF8))
        self.dbNameLabel.setText(QtGui.QApplication.translate("dbOptionsDialog", "Database name:", None, QtGui.QApplication.UnicodeUTF8))

