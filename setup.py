import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
# build_exe_options =
includefiles = ['db_credentials.txt',
                'interface_setup.txt',
                'Logo.png',
                'Bus.ico',
                'DejaVuSans.ttf',
                'DejaVuSans-Bold.ttf',
                'signature.txt']
# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(name="Travel Manager",
      version="1.7",
      description="Tickets and trips managing system.",
      options={"build_exe": {"packages": ["os"], "include_files": includefiles},
               "bdist_msi": {"upgrade_code": "oR9dkW345hNs92",
                             'data': {"Shortcut": [
                                 ("Travel Manager",        # Shortcut
                                  "DesktopFolder",          # Directory_
                                  "Travel Manager",           # Name
                                  "TARGETDIR",              # Component_
                                  "[TARGETDIR]TravelManager.exe",# Target
                                  None,                     # Arguments
                                  None,                     # Description
                                  None,                     # Hotkey
                                  None,                     # Icon
                                  None,                     # IconIndex
                                  None,                     # ShowCmd
                                  'TARGETDIR'               # WkDir
                                  )
                             ]}}},
      executables=[Executable("structure.py",
                              base=base,
                              icon="Bus.ico",
                              targetName="TravelManager.exe")])