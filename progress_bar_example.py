from PySide import QtGui, QtCore


class MyProgressBar(QtGui.QProgressBar):
    """
    Progress bar in busy mode with text displayed at the center.
    """

    def __init__(self):
        super().__init__()
        self.setRange(0, 0)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self._text = None

    def setText(self, text):
        self._text = text

    def text(self):
        return self._text

app = QtGui.QApplication([])

p = MyProgressBar()
p.setText('finding resource...')
p.show()

app.exec_()