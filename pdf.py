from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, inch
from reportlab.platypus import Table, TableStyle
from reportlab.lib import colors
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
import json


def pdf_utility(outpuFileName, tableData, textlist):
    pdfmetrics.registerFont(TTFont('DejaVuSans', 'DejaVuSans.ttf'))
    pdfmetrics.registerFont(TTFont('DejaVuSans-Bold', 'DejaVuSans-Bold.ttf'))

    listColumn = ['Time', 'Pickup', 'Hotel', 'Room', 'Name', 'Number', 'Ad', 'Ch', 'In', 'Lang', 'PoB', 'Vendor', 'Comments']

    width, height = A4
    # ---------------------------------------------
    # A4 page is 8.27x11.69 inches
    # 1pt in reportLab = 1/72 inches
    # the cursor start from the bottom of the page
    # ----------------------------------------------

    # ----------------------------------------------
    # JSON
    # ----------------------------------------------
    # with open(outpuFileName + '.txt', 'wb')as f:
    #     f.write(json.dumps(tableData, sort_keys=True, indent=4, separators=(',', ':')).encode('UTF-8'))
    # ----------------------------------------------
    can = canvas.Canvas(outpuFileName + '.pdf', pagesize=(11.69 * inch, 8.27 * inch))

    def inchToPoint(x):
        return 72*x

    # -----Calculate rows ------------------------
    table_rows = 28 - len(textlist)

    # --------------------------------------------
    # Inputs
    # --------------------------------------------
    a = int(len(tableData) / table_rows)
    pages_holder = list()
    it = 0
    while a >= 0:
        pages_holder.append(tableData[it:it + table_rows])
        it = it + table_rows
        a = a - 1
    # ---------Fonts and sizes--------------------
    textFontBottom = 'DejaVuSans-Bold'
    header_font = 'DejaVuSans-Bold'
    textFont = "DejaVuSans"
    textSizeBottom = 14
    textSize = 12
    cells_size = 8
    textColor = colors.black
    # --------------------------------------------
    # Positions
    # --------------------------------------------
    rowH = 0.25
    tableXPosition = inchToPoint(0.5)
    baseTableYPosition = inchToPoint(7.7)
    cells = [30, 120, 90, 30, 120, 30, 15, 15, 15, 25, 20, 125]

    for i in pages_holder[:-1]:
        data = i

        # --------------------------------------------
        # Text
        # --------------------------------------------
        textXPosition = inchToPoint(0.5)
        textYPosition = inchToPoint(7.7)
        betweenLines = inchToPoint(0.2)
        textHeight = len(textlist) * (betweenLines) + inchToPoint(0.6)
        iter_text = 0
        for tec in textlist:
            can.setFillColor(textColor)
            can.setFont(textFont, textSize)
            can.drawString(textXPosition, textYPosition - betweenLines * iter_text, tec)
            iter_text += 1

        # ---Change table position according text height
        tableYPosition = baseTableYPosition - textHeight + inchToPoint(rowH)
        # --------------------------------------------
        # Table
        # --------------------------------------------


        table = Table([listColumn], colWidths=cells,
                      rowHeights=[rowH * inch])
        table.setStyle(TableStyle(
            [('ROWBACKGROUNDS', (0, 0), (-1, -1), [colors.white]),
             ('GRID', (0, 0), (-1, -1), 1, colors.black),
             ('TEXTCOLOR', (0, 0), (-1, -1), colors.black),
             ('ALIGNMENT', (0, 0), (-1, -1), 'CENTER'),
             ('SIZE', (0, 0), (-1, -1), cells_size),
             ('FONTNAME', (0, 0), (-1, -1), header_font)]))
        table.wrapOn(can, width, height)
        table.drawOn(can, tableXPosition, tableYPosition - rowH)

        table = Table(data, colWidths=cells,
                      rowHeights=[rowH * inch] * len(data))
        table.setStyle(TableStyle([('ROWBACKGROUNDS', (0, 0), (-1, -1), [colors.white, colors.lightgrey]),
                                   ('GRID', (0, 0), (-1, -1), 1, colors.black),
                                   ('TEXTCOLOR', (0, 0), (1, -1), colors.black),
                                   ('ALIGNMENT', (0, 0), (-1, -1), 'CENTER'),
                                   ('SIZE', (0, 0), (-1, -1), cells_size)]))
        table.wrapOn(can, width, height)
        table.drawOn(can, tableXPosition, tableYPosition - rowH * len(data) * inch)

        #               Generate PDF
        # -----------------------------------------------

        can.showPage()

    data = pages_holder[-1:][0]
    # ---------------Text for last page----------------
    textXPosition = inchToPoint(0.5)
    textYPosition = inchToPoint(7.7)
    betweenLines = inchToPoint(0.2)
    textHeight = len(textlist) * (betweenLines) + inchToPoint(0.6)
    iter_text = 0
    for tec in textlist:
        can.setFillColor(textColor)
        can.setFont(textFont, textSize)
        can.drawString(textXPosition, textYPosition - betweenLines * iter_text, tec)
        iter_text += 1
    tableYPosition = baseTableYPosition - textHeight + rowH
    # ---------------TABLE for last page---------------

    table = Table([listColumn], colWidths=cells, rowHeights=[rowH * inch])
    table.setStyle(TableStyle(
        [('ROWBACKGROUNDS', (0, 0), (-1, -1), [colors.white]),
         ('GRID', (0, 0), (-1, -1), 1, colors.black),
         ('TEXTCOLOR', (0, 0), (-1, -1), colors.black),
         ('ALIGNMENT', (0, 0), (-1, -1), 'CENTER'),
         ('SIZE', (0, 0), (-1, -1), cells_size),
         ('FONTNAME', (0, 0), (-1, -1), header_font)]))
    table.wrapOn(can, width, height)
    table.drawOn(can, tableXPosition, tableYPosition - rowH)

    # ---------------TABLE for last page---------------

    table = Table(data, colWidths=cells,
                  rowHeights=[rowH * inch] * len(data))
    table.setStyle(TableStyle([('ROWBACKGROUNDS', (0, 0), (-1, -1), [colors.white, colors.lightgrey]),
                               ('GRID', (0, 0), (-1, -1), 1, colors.black),
                               ('TEXTCOLOR', (0, 0), (1, -1), colors.black),
                               ('ALIGNMENT', (0, 0), (-1, -1), 'CENTER'),
                               ('SIZE', (0, 0), (-1, -1), cells_size)]))
    table.wrapOn(can, width, height)
    table.drawOn(can, tableXPosition, tableYPosition - rowH * len(data) * inch)

    can.setFont(textFontBottom, textSizeBottom)
    can.drawString(inchToPoint(9.5), (tableYPosition - (rowH * len(data) + rowH) * inch),
                   'Total: ' + str(len(tableData)))
    can.showPage()
    can.save()


def pdf_finance(outpuFileName, tableData, price_summ, textlist):
    listColumn = ['Vendor', 'Date', 'Route', 'Number', 'Lan', 'AD', 'CH', 'IN', 'Price', 'PoB']
    pdfmetrics.registerFont(TTFont('DejaVuSans', 'DejaVuSans.ttf'))
    pdfmetrics.registerFont(TTFont('DejaVuSans-Bold', 'DejaVuSans-Bold.ttf'))
    width, height = A4
    # ---------------------------------------------
    # A4 page is 8.27x11.69 inches
    # 1pt in reportLab = 1/72 inches
    # the cursor start from the bottom of the page
    # ----------------------------------------------

    # ----------------------------------------------
    # JSON
    # ----------------------------------------------

    # with open(outpuFileName + '.txt', 'wb')as f:
    #     f.write(json.dumps(tableData, sort_keys=True, indent=4, separators=(',', ':')).encode('UTF-8'))

    # ----------------------------------------------

    can = canvas.Canvas(outpuFileName + '.pdf', pagesize=(11.69 * inch, 8.27 * inch))
    # can = canvas.Canvas(outpuFileName + '.pdf', pagesize=(8.27 * inch, 11.69 * inch))

    def inchToPoint(x):
        return 72 * x

    # --------------------------------------------
    # Inputs
    # --------------------------------------------
    table_rows = 28 - len(textlist)
    a = int(len(tableData) / table_rows)
    pages_holder = list()
    it = 0
    while a >= 0:
        pages_holder.append(tableData[it:it + table_rows])
        it = it + table_rows
        a = a - 1
    # ---------Fonts and sizes--------------------
    textFontBottom = 'DejaVuSans-Bold'
    header_font = 'DejaVuSans-Bold'
    textFont = 'DejaVuSans'
    textSizeBottom = 14
    textSize = 12
    cells_size = 8
    textColor = colors.black


    # --------------------------------------------
    # Positions
    # --------------------------------------------
    rowH = 0.25
    tableXPosition = inchToPoint(0.5)
    baseTableYPosition = inchToPoint(7.7)
    cells = [205, 70, 265, 40, 25, 25, 25, 25, 40, 40]
    for i in pages_holder[:-1]:
        data = i
        # --------------------------------------------
        # Text
        # --------------------------------------------
        textXPosition = inchToPoint(0.5)
        textYPosition = inchToPoint(7.7)
        betweenLines = inchToPoint(0.2)
        textHeight = len(textlist) * (betweenLines) + inchToPoint(0.6)
        iter_text = 0
        for tec in textlist:
            can.setFillColor(textColor)
            can.setFont(textFont, textSize)
            can.drawString(textXPosition, textYPosition - betweenLines * iter_text, tec)
            iter_text += 1

        # ---Change table position according text height
        tableYPosition = baseTableYPosition - textHeight + inchToPoint(rowH)
        # --------------------------------------------
        # Table
        # --------------------------------------------


        table = Table([listColumn], colWidths=cells,
                      rowHeights=[rowH * inch])
        table.setStyle(TableStyle(
            [('ROWBACKGROUNDS', (0, 0), (-1, -1), [colors.white]),
             ('FONTNAME', (0, 0), (-1, -1), header_font),
             ('GRID', (0, 0), (-1, -1), 1, colors.black),
             ('TEXTCOLOR', (0, 0), (-1, -1), colors.black),
             ('ALIGNMENT', (0, 0), (-1, -1), 'CENTER'),
             ('SIZE', (0, 0), (-1, -1), cells_size)]))
        table.wrapOn(can, width, height)
        table.drawOn(can, tableXPosition, tableYPosition - rowH)

        table = Table(data, colWidths=cells,
                      rowHeights=[rowH * inch] * len(data))
        table.setStyle(TableStyle([('ROWBACKGROUNDS', (0, 0), (-1, -1), [colors.white, colors.lightgrey]),
                                   ('GRID', (0, 0), (-1, -1), 1, colors.black),
                                   ('TEXTCOLOR', (0, 0), (1, -1), colors.black),
                                   ('ALIGNMENT', (0, 0), (-1, -1), 'CENTER'),
                                   ('SIZE', (0, 0), (-1, -1), cells_size)]))
        table.wrapOn(can, width, height)
        table.drawOn(can, tableXPosition, tableYPosition - rowH * len(data) * inch)

        # Generate PDF
        # -----------------------------------------------

        can.showPage()

    data = pages_holder[-1:][0]
    # --------------------------------------------
    # Text
    # --------------------------------------------
    if data:
        textXPosition = inchToPoint(0.5)
        textYPosition = inchToPoint(7.7)
        betweenLines = inchToPoint(0.2)
        textHeight = len(textlist) * (betweenLines) + inchToPoint(0.6)
        iter_text = 0
        for tec in textlist:
            can.setFillColor(textColor)
            can.setFont(textFont, textSize)
            can.drawString(textXPosition, textYPosition - betweenLines * iter_text, tec)
            iter_text += 1

        # ---Change table position according text height
        tableYPosition = baseTableYPosition - textHeight + rowH + rowH

        # ---------------TABLE for last page---------------

        table = Table([listColumn], colWidths=cells, rowHeights=[rowH * inch])
        table.setStyle(TableStyle(
            [('ROWBACKGROUNDS', (0, 0), (-1, -1), [colors.white]),
             ('GRID', (0, 0), (-1, -1), 1, colors.black),
             ('TEXTCOLOR', (0, 0), (-1, -1), colors.black),
             ('ALIGNMENT', (0, 0), (-1, -1), 'CENTER'),
             ('FONTNAME', (0, 0), (-1, -1), header_font),
             ('SIZE', (0, 0), (-1, -1), cells_size)]))
        table.wrapOn(can, width, height)
        table.drawOn(can, tableXPosition, tableYPosition - rowH)

        # ---------------TABLE for last page---------------

        table = Table(data, colWidths=cells,
                      rowHeights=[rowH * inch] * len(data))
        table.setStyle(TableStyle([('ROWBACKGROUNDS', (0, 0), (-1, -1), [colors.white, colors.lightgrey]),
                                   ('GRID', (0, 0), (-1, -1), 1, colors.black),
                                   ('TEXTCOLOR', (0, 0), (1, -1), colors.black),
                                   ('ALIGNMENT', (0, 0), (-1, -1), 'CENTER'),
                                   ('SIZE', (0, 0), (-1, -1), cells_size)]))
        table.wrapOn(can, width, height)
        table.drawOn(can, tableXPosition, tableYPosition - rowH * len(data) * inch)

    can.setFont(textFontBottom, textSizeBottom)
    can.drawString(inchToPoint(9.5), (tableYPosition - (rowH * len(data) + rowH) * inch),
                       'Total: ' + price_summ)
    can.showPage()

    can.save()
