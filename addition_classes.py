from PySide.QtCore import *
from PySide.QtGui import *
import newdriverdialog_ui
import newguidedialog_ui
import newvendordialog_ui
import databaseoptions_ui
import newbusdialog_ui
import newlangdialog_ui
import newhoteldialog_ui
import newpickupdialog_ui
import classes
import confirmations
import json
import tables
import newroutedialog_ui
import newtripdialog_ui
import ticketedit_ui
import datetime
import sigchange_ui

class DBOptions(QDialog, databaseoptions_ui.Ui_dbOptionsDialog):
    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(400, 145)
        with open('db_credentials.txt') as dbcred:
            creddict = json.load(dbcred)
            self.dbUserLineEdit.setText(creddict["db_user"])
            self.dbHostLineEdit.setText(creddict["db_host"])
            self.dbPWLineEdit.setEchoMode(QLineEdit.Password)
            self.dbPWLineEdit.setText(creddict["db_pass"])
            self.dbNameLineEdit.setText(creddict["db_name"])
        self.dbOptionsButtonBox.buttons()[0].setStyleSheet("background-color: lightgreen")
        self.dbOptionsButtonBox.accepted.connect(self.saveOptions)
        self.dbOptionsButtonBox.rejected.connect(self.reject)

    def saveOptions(self):
        db_cred = {"db_user": self.dbUserLineEdit.text(),
                   "db_pass": self.dbPWLineEdit.text(),
                   "db_host": self.dbHostLineEdit.text(),
                   "db_name": self.dbNameLineEdit.text()}
        if db_cred["db_user"] != "" and db_cred["db_host"] != "" and db_cred["db_pass"] != "" and db_cred[
            "db_name"] != "":
            with open('db_credentials.txt', 'w') as outfile:
                json.dump(db_cred, outfile)
            self.accept()
        else:
            self.reject()


class AddNewDriver(QDialog, newdriverdialog_ui.Ui_new_driver_dialog):
    def __init__(self, engine):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(self.width(), self.height())
        self.engine = engine
        self.buttonbox_driver.buttons()[0].setStyleSheet("background-color: lightgreen")
        self.buttonbox_driver.accepted.connect(self.save_driver)
        self.buttonbox_driver.rejected.connect(self.reject)

    def save_driver(self):
        newdr = {"comments": self.lineedit_driver_comments.text(),
                 "name": self.lineedit_driver_name.text(),
                 "phone": self.lineedit_driver_phone.text()}
        if newdr["name"] != "":
            s = classes.create_session(self.engine)
            s.add(classes.Driver(name=newdr["name"],
                                 phone=newdr["phone"],
                                 comments=newdr["comments"]))
            classes.kill_session(s)
            self.accept()
        else:
            self.reject()


class EditDriver(AddNewDriver):
    def __init__(self, engine, driver_id=int):
        AddNewDriver.__init__(self, engine)
        self.setWindowTitle("Edit Driver")
        self.id = driver_id
        self.s = classes.create_session(engine)
        self.n = self.s.query(classes.Driver).filter_by(id=self.id).one()
        self.lineedit_driver_name.setText(self.n.name)
        self.lineedit_driver_phone.setText(self.n.phone)
        self.lineedit_driver_comments.setText(self.n.comments)
        button_delete = self.buttonbox_driver.addButton("Delete", QDialogButtonBox.DestructiveRole)
        button_delete.clicked.connect(self.confirm_deletion)
        self.buttonbox_driver.rejected.connect(self.reject_func)

    def confirm_deletion(self):
        f = confirmations.confirm_deletion()
        if f:
            self.s.delete(self.n)
            classes.kill_session(self.s)
            self.accept()

    def save_driver(self):
        newdriver = {"name": self.lineedit_driver_name.text(),
                     "phone": self.lineedit_driver_phone.text(),
                     "comments": self.lineedit_driver_comments.text()}
        flag = True
        if newdriver["name"] in ["", None, 0]:
            flag = False
        if flag:
            self.n.name = newdriver["name"]
            self.n.phone = newdriver["phone"]
            self.n.comments = newdriver["comments"]
            classes.kill_session(self.s)
            self.accept()
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Error")
            msg.setText("A driver should have a name!")
            msg.exec_()

    def reject_func(self):
        classes.kill_session(self.s)
        self.reject()


class AddNewGuide(QDialog, newguidedialog_ui.Ui_new_guide_dialog):
    def __init__(self, engine):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(self.width(), self.height())
        self.engine = engine
        self.buttonbox_guide.buttons()[0].setStyleSheet("background-color: lightgreen")
        self.buttonbox_guide.accepted.connect(self.save_guide)
        self.buttonbox_guide.rejected.connect(self.reject)

    def save_guide(self):
        newguide = {"comments": self.lineedit_guide_comments.text(),
                    "name": self.lineedit_guide_name.text(),
                    "phone": self.lineedit_guide_phone.text()}
        if newguide["name"] != "":
            s = classes.create_session(self.engine)
            s.add(classes.Guide(name=newguide["name"],
                                phone=newguide["phone"],
                                comments=newguide["comments"]))
            classes.kill_session(s)
            self.accept()
        else:
            self.reject()


class EditGuide(AddNewGuide):
    def __init__(self, engine, guide_id=int):
        AddNewGuide.__init__(self, engine)
        self.setWindowTitle("Edit Guide")
        self.id = guide_id
        self.s = classes.create_session(engine)
        self.n = self.s.query(classes.Guide).filter_by(id=self.id).one()
        self.lineedit_guide_name.setText(self.n.name)
        self.lineedit_guide_phone.setText(self.n.phone)
        self.lineedit_guide_comments.setText(self.n.comments)
        button_delete = self.buttonbox_guide.addButton("Delete", QDialogButtonBox.DestructiveRole)
        button_delete.clicked.connect(self.confirm_deletion)
        self.buttonbox_guide.rejected.connect(self.reject_func)

    def confirm_deletion(self):
        f = confirmations.confirm_deletion()
        if f:
            self.s.delete(self.n)
            classes.kill_session(self.s)
            self.accept()

    def save_guide(self):
        newguide = {"name": self.lineedit_guide_name.text(),
                    "phone": self.lineedit_guide_phone.text(),
                    "comments": self.lineedit_guide_comments.text()}
        flag = True
        if newguide["name"] in ["", None, 0]:
            flag = False
        if flag:
            self.n.name = newguide["name"]
            self.n.phone = newguide["phone"]
            self.n.comments = newguide["comments"]
            classes.kill_session(self.s)
            self.accept()
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Error")
            msg.setText("A guide should have a name!")
            msg.exec_()

    def reject_func(self):
        classes.kill_session(self.s)
        self.reject()


class AddNewVendor(QDialog, newvendordialog_ui.Ui_new_vendor_dialog):
    def __init__(self, engine):
        QDialog.__init__(self)
        self.setupUi(self)
        self.engine = engine
        self.setModal(True)
        self.buttonbox_vendor.buttons()[0].setStyleSheet("background-color: lightgreen")
        self.setFixedSize(self.width(), self.height())
        self.buttonbox_vendor.accepted.connect(self.save_vendor)
        self.buttonbox_vendor.rejected.connect(self.reject)

    def save_vendor(self):
        newven = {"name": self.lineedit_vendor_name.text(),
                  "afm": self.lineedit_vendor_afm.text(),
                  "fax": self.lineedit_vendor_fax.text(),
                  "phone": self.linenedit_vendor_phone.text(),
                  "email": self.lineedit_vendor_email.text()}
        flag = True
        if newven["name"] == "":
            flag = False
        if flag:
            s = classes.create_session(self.engine)
            s.add(classes.Vendor(name=newven["name"],
                                 afm=newven["afm"],
                                 phone=newven["phone"],
                                 fax=newven["fax"],
                                 email=newven["email"]))
            classes.kill_session(s)
            self.accept()
        else:
            self.reject()


class EditVendor(AddNewVendor):
    def __init__(self, engine, vendor_id):
        AddNewVendor.__init__(self, engine)
        self.setWindowTitle("Edit Rep")
        self.vid = vendor_id
        self.s = classes.create_session(engine)
        self.n = self.s.query(classes.Vendor).filter_by(id=self.vid).one()
        self.lineedit_vendor_name.setText(self.n.name)
        self.lineedit_vendor_afm.setText(self.n.afm)
        self.linenedit_vendor_phone.setText(self.n.phone)
        self.lineedit_vendor_fax.setText(self.n.fax)
        self.lineedit_vendor_email.setText(self.n.email)
        button_delete = self.buttonbox_vendor.addButton("Delete", QDialogButtonBox.DestructiveRole)
        button_delete.clicked.connect(self.confirm_deletion)
        self.buttonbox_vendor.rejected.connect(self.reject_func)

    def confirm_deletion(self):
        f = confirmations.confirm_deletion()
        if f:
            self.s.delete(self.n)
            classes.kill_session(self.s)
            self.accept()

    def save_vendor(self):
        newven = {"name": self.lineedit_vendor_name.text(),
                  "afm": self.lineedit_vendor_afm.text(),
                  "fax": self.lineedit_vendor_fax.text(),
                  "phone": self.linenedit_vendor_phone.text(),
                  "email": self.lineedit_vendor_email.text()}
        flag = newven["name"] != ""
        if flag:
            self.n.name = newven["name"]
            self.n.afm = newven["afm"]
            self.n.fax = newven["fax"]
            self.n.phone = newven["phone"]
            self.n.email = newven["email"]

            classes.kill_session(self.s)
            self.accept()
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Error")
            msg.setText("A rep should have a name!")
            msg.exec_()

    def reject_func(self):
        classes.kill_session(self.s)
        self.reject()


class AddNewBus(QDialog, newbusdialog_ui.Ui_new_bus_dialog):
    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(self.width(), self.height())
        self.buttonbox_bus.buttons()[0].setStyleSheet("background-color: lightgreen")
        self.buttonbox_bus.accepted.connect(self.save_bus)
        self.buttonbox_bus.rejected.connect(self.reject)
        self.result = None

    def save_bus(self):
        newbus = {"name": self.lineedit_bus_name.text(),
                  "cap": self.lineedit_bus_capacity.value()}
        flag = True
        for i in newbus:
            if newbus[i] in ["", None, 0]:
                flag = False
        if flag:
            self.result = classes.Bus(name=newbus["name"], capacity=newbus["cap"])
            self.accept()
        else:
            self.reject()


class EditBus(AddNewBus):
    def __init__(self, engine, bus_id=int):
        AddNewBus.__init__(self)
        self.setWindowTitle("Edit Bus")
        self.bid = bus_id
        self.s = classes.create_session(engine)
        self.n = self.s.query(classes.Bus).filter_by(id=self.bid).first()
        self.lineedit_bus_name.setText(self.n.name)
        self.lineedit_bus_capacity.setValue(self.n.capacity)
        button_delete = self.buttonbox_bus.addButton("Delete", QDialogButtonBox.DestructiveRole)
        button_delete.clicked.connect(self.confirm_deletion)
        self.buttonbox_bus.rejected.connect(self.reject_func)

    def confirm_deletion(self):
        f = confirmations.confirm_deletion()
        if f:
            self.s.delete(self.n)
            classes.kill_session(self.s)
            self.accept()

    def save_bus(self):
        newbus = {"name": self.lineedit_bus_name.text(),
                  "cap": self.lineedit_bus_capacity.value()}
        flag = True
        for i in newbus:
            if newbus[i] in ["", None, 0]:
                flag = False
        if flag:
            self.n.name = newbus["name"]
            self.n.capacity = newbus["cap"]
            classes.kill_session(self.s)
            self.accept()
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Error")
            msg.setText("A bus should have a name and a capacity!")
            msg.exec_()

    def reject_func(self):
        classes.kill_session(self.s)
        self.reject()


class AddNewLanguage(QDialog, newlangdialog_ui.Ui_new_lang_dialog):
    def __init__(self, engine):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(self.width(), self.height())
        self.engine = engine
        self.init_langs()
        self.buttonbox_lang.buttons()[0].setStyleSheet("background-color: lightgreen")
        self.buttonbox_lang.accepted.connect(self.save_lang)
        self.buttonbox_lang.rejected.connect(self.reject)
        self.button_del.clicked.connect(self.delete_lang)

    def save_lang(self):
        newlang = {"name": self.lineedit_lang_name.text()}
        flag = True
        for i in newlang:
            if newlang[i] in ["", None]:
                flag = False
        if flag:
            s = classes.create_session(self.engine)
            s.add(classes.Language(name=newlang["name"]))
            classes.kill_session(s)
            self.accept()
        else:
            self.reject()

    def init_langs(self):
        self.combobox_language.clear()
        s = classes.create_session(self.engine)
        n = s.query(classes.Language).all()
        for i in n:
            self.combobox_language.addItem(i.name, userData=i.id)
        classes.kill_session(s)

    def delete_lang(self):
        if self.combobox_language.currentIndex() == -1:
            return
        s = classes.create_session(self.engine)
        id = self.combobox_language.itemData(self.combobox_language.currentIndex())
        n = s.query(classes.Language).filter_by(id=id).first()
        s.delete(n)
        classes.kill_session(s)
        self.init_langs()


class AddNewHotel(QDialog, newhoteldialog_ui.Ui_new_hotel_dialog):
    def __init__(self, engine):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(self.width(), self.height())
        self.engine = engine
        self.init_pickups()
        self.buttonbox_hotel.buttons()[0].setStyleSheet("background-color: lightgreen")
        self.buttonbox_hotel.accepted.connect(self.save_hotel)
        self.buttonbox_hotel.rejected.connect(self.reject)

    def save_hotel(self):
        newhotel = {"name": self.lineedit_hotel_name.text(),
                    "comments": self.lineedit_hotel_comments.text()}
        flag = True
        if newhotel["name"] in ["", None]:
            flag = False
        if flag:
            s = classes.create_session(self.engine)
            if self.combobox_pickups.currentIndex() == -1:
                s.add(classes.Hotel(name=newhotel["name"],
                                    comments=newhotel["comments"]))
            else:
                tmp = self.combobox_pickups.itemData(self.combobox_pickups.currentIndex())
                s.add(classes.Hotel(name=newhotel["name"],
                                    comments=newhotel["comments"],
                                    pickup_points_id=tmp))
            classes.kill_session(s)
            self.accept()
        else:
            self.reject()

    def init_pickups(self):
        s = classes.create_session(self.engine)
        n = s.query(classes.Pickup_Point).all()
        self.combobox_pickups.clear()
        for i in n:
            self.combobox_pickups.addItem(i.name, userData=i.id)
        classes.kill_session(s)
        self.combobox_pickups.setCurrentIndex(-1)


class EditHotel(AddNewHotel):
    def __init__(self, engine, n):
        AddNewHotel.__init__(self, engine)
        self.engine = engine
        self.id = n
        s = classes.create_session(self.engine)
        h = s.query(classes.Hotel).filter_by(id=self.id).first()
        pu = h.pickup_points_id
        self.lineedit_hotel_name.setText(h.name)
        self.lineedit_hotel_comments.setText(h.comments)
        classes.kill_session(s)
        self.combobox_pickups.setCurrentIndex(self.combobox_pickups.findData(pu))
        self.del_button = self.buttonbox_hotel.addButton("Delete", QDialogButtonBox.DestructiveRole)
        self.del_button.clicked.connect(self.delete_hotel)
        self.buttonbox_hotel.accepted.connect(self.save_changes)

    def save_changes(self):
        if self.lineedit_hotel_name.text() == "":
            return
        s = classes.create_session(self.engine)
        h = s.query(classes.Hotel).filter_by(id=self.id)
        h.name = self.lineedit_hotel_name.text()
        h.comments = self.lineedit_hotel_comments.text()
        tmp = self.combobox_pickups.currentIndex()
        if tmp != -1:
            h.pickup_points_id = self.combobox_pickups.itemData(tmp)
        classes.kill_session(s)
        self.accept()


    def delete_hotel(self):
        if confirmations.confirm_deletion():
            s = classes.create_session(self.engine)
            h = s.query(classes.Hotel).filter_by(id=self.id).first()
            s.delete(h)
            classes.kill_session(s)
            self.accept()
        return


class AddNewPickupPoint(QDialog, newpickupdialog_ui.Ui_newpickup_dialog):
    def __init__(self, engine, pid=None):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(self.width(), self.height())
        self.engine = engine
        self.init_routes(pid)
        self.tableview_routes.setModel(self.mdl)
        font = QFont("Verdana", 9)
        self.tableview_routes.setFont(font)
        self.tableview_routes.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tableview_routes.resizeColumnsToContents()
        self.tableview_routes.setSortingEnabled(True)
        self.listwidg_altnames.itemDoubleClicked.connect(self.del_altname)
        self.toolbutton_plus.clicked.connect(self.add_altname)
        self.newpickup_buttonbox.buttons()[0].setStyleSheet("background-color: lightgreen")
        self.newpickup_buttonbox.accepted.connect(self.save_pickuppoint)
        self.newpickup_buttonbox.rejected.connect(self.reject)
        self.tableview_routes.doubleClicked.connect(self.change_dt)

    def change_dt(self, ind):
        (trash, dt) = self.mdl.get_id_t(ind)
        d, boo = QInputDialog.getInt(self, 'Edit time difference',
                                     'Minutes after start of the trip:',
                                     value=dt)
        # print(d)
        self.mdl.set_dt(ind.row(), d)

    def init_routes(self, pid=None):
        s = classes.create_session(self.engine)
        routes_list = s.query(classes.Route).all()
        self.routes_dict = []
        if pid:
            for i in routes_list:
                self.routes_dict.append([i.id,
                                         i.name,
                                         s.query(classes.Pickup_Time).filter_by(routes=i,
                                                                                pickup_points_id=pid).first().time,
                                         QTime(i.start_time)])
        else:
            for i in routes_list:
                self.routes_dict.append([i.id, i.name, 0, QTime(i.start_time)])
        classes.kill_session(s)
        self.mdl = tables.RoutesTable(self.routes_dict)

    def add_altname(self):
        t = self.lineedit_altname.text()
        self.lineedit_altname.clear()
        QListWidgetItem(t, self.listwidg_altnames)

    def del_altname(self):
        self.listwidg_altnames.takeItem(self.listwidg_altnames.currentRow())

    def save_pickuppoint(self):
        newpickuppoint = {"name": self.lineedit_name.text()}
        flag = True
        if newpickuppoint["name"] in ["", None]:
            flag = False
        if flag:
            altnames = []
            while True:
                f = self.listwidg_altnames.takeItem(0)
                if f.__class__.__name__ != "QListWidgetItem":
                    break
                altnames.append(f.text())
            altnames = json.dumps(altnames)
            i = 0
            routes_times = []
            while True:
                d, t = self.mdl.get_id_t(self.mdl.createIndex(i, 0))
                if d is None:
                    break
                i += 1
                routes_times.append((d, t))
            s = classes.create_session(self.engine)
            pp = classes.Pickup_Point(name=newpickuppoint["name"], list_names=altnames)
            s.add(pp)
            s.commit()
            for i in routes_times:
                t = classes.Pickup_Time(time=i[1], pickup_points_id=pp.id, routes_id=i[0])
                s.add(t)
            classes.kill_session(s)
            self.accept()
        else:
            self.reject()


class EditPickup(AddNewPickupPoint):
    def __init__(self, engine, pid):
        AddNewPickupPoint.__init__(self, engine, pid)
        self.pid = pid
        self.setWindowTitle("Edit Pickup Point")
        del_button = self.newpickup_buttonbox.addButton("Delete", QDialogButtonBox.DestructiveRole)
        del_button.clicked.connect(self.delete_pickup)
        self.newpickup_buttonbox.accepted.disconnect(self.save_pickuppoint)
        self.newpickup_buttonbox.accepted.connect(self.save_edited_pickup)
        s = classes.create_session(self.engine)
        p = s.query(classes.Pickup_Point).filter_by(id=self.pid).first()
        self.lineedit_name.setText(p.name)
        altn = json.loads(p.list_names)
        for i in altn:
            self.listwidg_altnames.addItem(i)
            classes.kill_session(s)


    def delete_pickup(self):
        if confirmations.confirm_deletion():
            s = classes.create_session(self.engine)
            n = s.query(classes.Pickup_Point).filter_by(id=self.pid).first()
            times = s.query(classes.Pickup_Time).filter_by(pickup_points_id=self.pid).all()
            for i in times:
                s.delete(i)
            s.delete(n)
            classes.kill_session(s)
            self.accept()
        return

    def save_edited_pickup(self):
        if confirmations.confirm_editing() == QMessageBox.Rejected:
            return
        s = classes.create_session(self.engine)
        pu = s.query(classes.Pickup_Point).filter_by(id=self.pid).first()
        flag = True
        if self.lineedit_name.text() in ["", None]:
            flag = False
        if flag:
            altnames = []
            while True:
                f = self.listwidg_altnames.takeItem(0)
                if f.__class__.__name__ != "QListWidgetItem":
                    break
                altnames.append(f.text())
            altnames = json.dumps(altnames)
            i = 0
            routes_times = []
            while True:
                d, t = self.mdl.get_id_t(self.mdl.createIndex(i, 0))
                if d is None:
                    break
                i += 1
                routes_times.append((d, t))
            pu.name = self.lineedit_name.text()
            pu.list_names = altnames
            s.commit()
            for i in routes_times:
                put = s.query(classes.Pickup_Time).filter_by(pickup_points_id=pu.id, routes_id=i[0]).first()
                put.time = i[1]
            s.commit()
            classes.kill_session(s)
            self.accept()
        classes.kill_session(s)
        self.reject()


class AddNewTrip(QDialog, newtripdialog_ui.Ui_trip_addition_dialog):
    def __init__(self, engine):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(self.width(), self.height())
        self.engine = engine
        self.dateTimeEdit.setDate(QDate.currentDate().addDays(1))
        self.init_buses()
        self.init_routes()
        self.init_guides()
        self.init_drivers()

        self.route_combobox.currentIndexChanged.connect(self.route_changed)
        self.trip_addition_buttonbox.buttons()[0].setStyleSheet("background-color: lightgreen")
        self.trip_addition_buttonbox.accepted.connect(self.save_trip)
        self.trip_addition_buttonbox.rejected.connect(self.reject)

    def init_buses(self):
        self.bus_combobox.clear()
        s = classes.create_session(self.engine)
        n = s.query(classes.Bus).all()
        for i in n:
            self.bus_combobox.addItem(i.name + " (" + i.capacity.__str__() + ")",
                                      userData=i.id)
        self.bus_combobox.setCurrentIndex(-1)
        classes.kill_session(s)

    def init_guides(self):
        self.guides_combobox.clear()
        s = classes.create_session(self.engine)
        n = s.query(classes.Guide).all()
        for i in n:
            self.guides_combobox.addItem(i.name,
                                         userData=i.id)
        self.guides_combobox.setCurrentIndex(-1)
        classes.kill_session(s)

    def init_drivers(self):
        self.drivers_combobox.clear()
        s = classes.create_session(self.engine)
        n = s.query(classes.Driver).all()
        for i in n:
            self.drivers_combobox.addItem(i.name,
                                          userData=i.id)
        self.drivers_combobox.setCurrentIndex(-1)
        classes.kill_session(s)

    def init_routes(self):
        self.route_combobox.clear()
        s = classes.create_session(self.engine)
        n = s.query(classes.Route).all()
        for i in n:
            self.route_combobox.addItem(i.name,
                                        userData=i.id)
        self.route_combobox.setCurrentIndex(-1)
        classes.kill_session(s)

    def route_changed(self):
        tmp = self.route_combobox.currentIndex()
        if tmp == -1:
            return
        s = classes.create_session(self.engine)
        id = self.route_combobox.itemData(tmp)
        r = s.query(classes.Route).filter_by(id=id).first()
        self.dateTimeEdit.setTime(r.start_time)
        classes.kill_session(s)

    def save_trip(self, new=True, tripid=-1):
        if (self.bus_combobox.currentIndex() == -1 or
                    self.route_combobox.currentIndex() == -1 or
                    self.guides_combobox.currentIndex() == -1 or
                    self.drivers_combobox.currentIndex() == -1):
            msg = QMessageBox()
            msg.setText("Fill all the necessary fields")
            msg.setWindowTitle("Saving failed")
            msg.setIcon(QMessageBox.Critical)
            msg.exec_()
            return
        when = self.dateTimeEdit.dateTime().toPython()
        d1 = datetime.datetime(when.year, when.month, when.day)
        d2 = d1 + datetime.timedelta(days=1)
        s = classes.create_session(self.engine)
        busid = self.bus_combobox.itemData(self.bus_combobox.currentIndex())
        guideid = self.guides_combobox.itemData(self.guides_combobox.currentIndex())
        driverid = self.drivers_combobox.itemData(self.drivers_combobox.currentIndex())
        if new:
            bus_avail = s.query(classes.Trip).filter(classes.Trip.start_date_time >= d1,
                                                     classes.Trip.start_date_time < d2,
                                                     classes.Trip.buses_id == busid).first()
            guide_avail = s.query(classes.Trip).filter(classes.Trip.start_date_time >= d1,
                                                       classes.Trip.start_date_time < d2,
                                                       classes.Trip.guides_id == guideid).first()
            driver_avail = s.query(classes.Trip).filter(classes.Trip.start_date_time >= d1,
                                                        classes.Trip.start_date_time < d2,
                                                        classes.Trip.drivers_id == driverid).first()
            if bus_avail != None:
                msg = QMessageBox(self)
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Bus is unavailable")
                msg.setText("The bus is going to " + bus_avail.routes.name + " on that day.")
                r = msg.exec_()
                classes.kill_session(s)
                return

            if guide_avail != None:
                msg = QMessageBox(self)
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Guide is unavailable")
                msg.setText("The guide is leading tourists on " + guide_avail.routes.name + " on that day.")
                r = msg.exec_()
                classes.kill_session(s)
                return

            if driver_avail != None:
                msg = QMessageBox(self)
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Driver is unavailable")
                msg.setText("The driver is driving a bus to " + driver_avail.routes.name + " on that day.")
                r = msg.exec_()
                classes.kill_session(s)
                return

        routeid = self.route_combobox.itemData(self.route_combobox.currentIndex())
        busobj = s.query(classes.Bus).filter_by(id=busid).first()
        if not new:
            t = s.query(classes.Trip).filter_by(id=tripid).first()
            if t == None:
                msg = QMessageBox(self)
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Trip not found")
                msg.setText("Error querying the trip from the database. Check your data.")
                r = msg.exec_()
                classes.kill_session(s)
                self.reject()
            t.routes_id = routeid
            t.drivers_id = driverid
            t.guides_id = guideid
            t.start_date_time = when
            t.buses_id = busid
            t.max_passengers = busobj.capacity
            classes.kill_session(s)
            self.accept()
        else:
            t = classes.Trip(routes_id=routeid,
                             drivers_id=driverid,
                             guides_id=guideid,
                             start_date_time=when,
                             buses_id=busid,
                             max_passengers=busobj.capacity)
            s.add(t)
            classes.kill_session(s)
            self.accept()
        classes.kill_session(s)


class EditTrip(AddNewTrip):
    def __init__(self, engine, id):
        AddNewTrip.__init__(self, engine)
        self.id = id
        self.engine = engine
        s = classes.create_session(self.engine)
        t = s.query(classes.Trip).filter_by(id=self.id).first()
        self.dateTimeEdit.setDateTime(t.start_date_time)
        self.route_combobox.setCurrentIndex(self.route_combobox.findData(t.routes_id))
        self.bus_combobox.setCurrentIndex(self.bus_combobox.findData(t.buses_id))
        self.drivers_combobox.setCurrentIndex(self.drivers_combobox.findData(t.drivers_id))
        self.guides_combobox.setCurrentIndex(self.guides_combobox.findData(t.guides_id))
        classes.kill_session(s)
        delbutton = self.trip_addition_buttonbox.addButton("Delete", QDialogButtonBox.DestructiveRole)
        delbutton.clicked.connect(self.delete_trip)
        self.trip_addition_buttonbox.accepted.disconnect(self.save_trip)
        self.trip_addition_buttonbox.accepted.connect(self.save_edited_trip)

    def save_edited_trip(self):
        self.save_trip(new=False, tripid=self.id)

    def delete_trip(self):
        if confirmations.confirm_trip_deletion():
            s = classes.create_session(self.engine)
            tickets = s.query(classes.Ticket).filter_by(trips_id=self.id).all()
            for i in tickets:
                s.delete(i)
            thetrip = s.query(classes.Trip).filter_by(id=self.id).first()
            s.delete(thetrip)
            classes.kill_session(s)
            self.accept()
        return


class AddNewRoute(QDialog, newroutedialog_ui.Ui_newroute_dialog):
    def __init__(self, engine):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(self.width(), self.height())
        self.engine = engine
        self.timeEdit.setTime(QTime(6, 0))
        time = self.timeEdit.time()
        self.init_pickups(time)
        self.newroute_buttonbox.buttons()[0].setStyleSheet("background-color: lightgreen")
        self.pickup_times_tableview.doubleClicked.connect(self.edit_dt)
        self.timeEdit.timeChanged.connect(self.pickup_model.new_time)
        self.newroute_buttonbox.accepted.connect(self.save_new)

    def edit_dt(self, ind):
        # print(ind)
        # print(ind.row())
        (trash, dt) = self.pickup_model.get_id_t(ind.row())
        d, boo = QInputDialog.getInt(self, 'Edit time difference',
                                     'Minutes after start of the trip:',
                                     value=dt)
        # print(d)
        self.pickup_model.set_dt(ind.row(), d)

    def init_pickups(self, time, rid=-1):
        s = classes.create_session(self.engine)
        pickups = s.query(classes.Pickup_Point).all()

        list_of_pickups = []
        for i in pickups:
            dt = 0
            if not rid == -1:
                # print(rid)
                t = s.query(classes.Pickup_Time).filter_by(pickup_points_id=i.id, routes_id=rid).first()
                dt = t.time
            list_of_pickups.append([i.id, i.name, dt])
        classes.kill_session(s)
        self.pickup_model = tables.PickupsTable(list_of_pickups, time, self)
        self.pickup_times_tableview.setModel(self.pickup_model)
        font = QFont("Verdana", 9)
        self.pickup_times_tableview.setFont(font)
        self.pickup_times_tableview.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.pickup_times_tableview.resizeColumnsToContents()
        self.pickup_times_tableview.setSortingEnabled(True)

    def save_new(self):
        if (self.lineedit_name.text() == "" or
                    self.priceEdit.value() == 0):
            return
        r = classes.Route(name=self.lineedit_name.text(),
                          price=self.priceEdit.value(),
                          start_time=self.timeEdit.time().toPython())
        s = classes.create_session(self.engine)
        s.add(r)
        s.commit()
        k = 0
        while True:
            pickupid, dt = self.pickup_model.get_id_t(k)
            if dt == None:
                break
            pt = classes.Pickup_Time(routes=r, pickup_points_id=pickupid, time=dt)
            s.add(pt)
            k += 1
        classes.kill_session(s)
        self.accept()


class EditRoute(AddNewRoute):
    def __init__(self, engine, rid):
        AddNewRoute.__init__(self, engine)
        self.rid = rid
        s = classes.create_session(self.engine)
        r = s.query(classes.Route).filter_by(id=rid).first()
        self.lineedit_name.setText(r.name)
        self.timeEdit.setTime(r.start_time)
        self.priceEdit.setValue(r.price)
        self.init_pickups(self.timeEdit.time(), rid=self.rid)
        classes.kill_session(s)
        del_button = self.newroute_buttonbox.addButton("Delete", QDialogButtonBox.DestructiveRole)
        del_button.clicked.connect(self.delete_item)

    def save_new(self):
        if (self.lineedit_name.text() == "" or
                    self.priceEdit.value() == 0):
            return
        s = classes.create_session(self.engine)
        r = s.query(classes.Route).filter_by(id=self.rid).first()
        r.name = self.lineedit_name.text()
        r.price = self.priceEdit.value()
        r.start_time = self.timeEdit.time().toPython()
        s.commit()
        k = 0
        while True:
            pickupid, dt = self.pickup_model.get_id_t(k)
            if dt == None:
                break
            ttmp = s.query(classes.Pickup_Time).filter_by(routes_id=r.id, pickup_points_id=pickupid).first()
            if ttmp != None:
                ttmp.time = dt
            else:
                pt = classes.Pickup_Time(routes=r, pickup_points_id=pickupid, time=dt)
                s.add(pt)
            k += 1
        classes.kill_session(s)
        self.accept()

    def delete_item(self):
        if confirmations.confirm_deletion():
            s = classes.create_session(self.engine)
            times = s.query(classes.Pickup_Time).filter_by(routes_id=self.rid).all()
            for i in times:
                s.delete(i)
            r = s.query(classes.Route).filter_by(id=self.rid).first()
            s.delete(r)
            classes.kill_session(s)
            self.accept()
        return


class EditTicket(QDialog, ticketedit_ui.Ui_editticket_dialog):
    def __init__(self, engine, id):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(self.width(), self.height())
        self.engine = engine
        self.id = id
        self.ticketPriceDoubleSpinBox.setMaximum(999999)
        self.doubleSpinBox.setMaximum(999999)
        self.init_view()
        self.pickup_point_changed()
        self.button_save.setDefault(True)
        self.button_save.setStyleSheet("background-color: lightgreen")
        self.button_reset.setText("Cancel")
        self.button_reset.clicked.connect(self.reject)
        self.button_save.clicked.connect(self.save_changes)
        self.delete_button.clicked.connect(self.delete_ticket)

    def init_view(self):
        s = classes.create_session(self.engine)
        t = s.query(classes.Ticket).filter_by(id=self.id).first()

        self.dateOfTheTrip.setDate(t.trips.start_date_time.date())
        self.lineedit_client.setText(t.customer)
        self.adultsSpinBox.setValue(t.adults)
        self.childrenSpinBox.setValue(t.children)
        self.infantsSpinBox.setValue(t.infants)
        self.roomNumberLineEdit.setText(t.room)
        self.voucherNumberLineEdit.setText(t.number)
        self.sale_date_edit.setDate(t.date)
        self.doubleSpinBox.setValue(t.pay_on_bus)
        self.commentsTextInput.setText(t.comments)

        hotels = s.query(classes.Hotel).all()
        pickups = s.query(classes.Pickup_Point).all()
        vendors = s.query(classes.Vendor).all()
        d1 = datetime.datetime(t.trips.start_date_time.year, t.trips.start_date_time.month, t.trips.start_date_time.day)
        d2 = d1 + datetime.timedelta(days=1)
        trips = s.query(classes.Trip).filter(classes.Trip.start_date_time >= d1,
                                             classes.Trip.start_date_time < d2).all()
        languages = s.query(classes.Language).all()
        for i in languages:
            self.languageComboBox.addItem(i.name, userData=i.id)
        self.languageComboBox.setCurrentIndex(self.languageComboBox.findData(t.languages_id))

        self.hotelCompleterList = []
        for i in hotels:
            self.hotelComboBox.addItem(i.name, userData=i.id)
            self.hotelCompleterList.append(i.name)
        self.hotelCompleter = QCompleter(self.hotelCompleterList, self)
        self.hotelCompleter.setCaseSensitivity(Qt.CaseInsensitive)
        self.hotelComboBox.setCompleter(self.hotelCompleter)
        self.hotelComboBox.setCurrentIndex(self.hotelComboBox.findData(t.hotels_id))
        for i in pickups:
            names = json.loads(i.list_names)
            for j in names:
                self.comboBox.addItem(i.name + ": " + j, userData=i.id)

        for i in vendors:
            self.chooseVendorComboBox.addItem(i.name, userData=i.id)

        self.date_changed()
        self.tripInput.setCurrentIndex(self.tripInput.findData(t.trips_id))
        self.route_changed()
        self.chooseVendorComboBox.setCurrentIndex(self.chooseVendorComboBox.findData(t.vendors_id))
        pun = self.comboBox.findText(t.pickup_point_name)
        if pun != -1:
            self.comboBox.setCurrentIndex(pun)
        else:
            self.comboBox.setCurrentIndex(self.comboBox.findData(t.pickup_points_id))
        classes.kill_session(s)

    def date_changed(self):
        d = self.dateOfTheTrip.date().toPython()
        convd = datetime.datetime(d.year, d.month, d.day)
        nextconvd = convd + datetime.timedelta(days=1)
        self.tripInput.clear()
        s = classes.create_session(self.engine)
        avail_trips = s.query(classes.Trip).filter(classes.Trip.start_date_time >= convd,
                                                   classes.Trip.start_date_time < nextconvd).all()
        for i in avail_trips:
            # print(i.routes.name + ": " + i.buses.name)
            self.tripInput.addItem(i.routes.name +
                                   ": " +
                                   i.buses.name +
                                   " (" +
                                   i.buses.capacity.__str__() +
                                   ")", userData=i.id)
        classes.kill_session(s)
        self.tripInput.setCurrentIndex(-1)

    def route_changed(self):
        if self.hotelComboBox.currentIndex() != -1:
            self.hotelChanged()
        elif self.comboBox.currentIndex() != -1:
            self.pickup_point_changed()
        self.peopleChanged()

    def hotelChanged(self):
        p = self.hotelComboBox.itemData(self.hotelComboBox.currentIndex())
        if p is None:
            return
        s = classes.create_session(self.engine)
        hotel = s.query(classes.Hotel).filter_by(id=p).first()
        i = self.comboBox.findData(hotel.pickup_points.id)
        self.comboBox.setCurrentIndex(i)
        classes.kill_session(s)
        self.pickup_point_changed()

    def pickup_point_changed(self):
        if self.tripInput.currentIndex() == -1:
            return
        pickup_id = self.comboBox.itemData(self.comboBox.currentIndex())
        tripindex = self.tripInput.currentIndex()
        trip_id = self.tripInput.itemData(tripindex)
        s = classes.create_session(self.engine)
        pickup = s.query(classes.Pickup_Point).filter_by(id=pickup_id).first()
        trip = s.query(classes.Trip).filter_by(id=trip_id).first()
        route = trip.routes
        time_diff = s.query(classes.Pickup_Time).filter_by(pickup_points=pickup, routes=route).first().time
        qstart_time = QDateTime(trip.start_date_time.date(), trip.start_date_time.time())
        actual_pickup_time = qstart_time.addSecs(time_diff * 60).time()
        classes.kill_session(s)
        self.pickupTimeLabel.setText("Pickup time: " + actual_pickup_time.toString("hh:mm"))

    def peopleChanged(self):
        i = self.tripInput.currentIndex()
        if i != -1:
            s = classes.create_session(self.engine)
            trip = s.query(classes.Trip).filter_by(id=self.tripInput.itemData(i)).first()
            pricePerCapita = trip.routes.price
            chldrn = self.childrenSpinBox.value()
            adlts = self.adultsSpinBox.value()
            totalPrice = adlts * pricePerCapita + (1 / 2) * chldrn * pricePerCapita
            self.ticketPriceDoubleSpinBox.setValue(totalPrice)
            classes.kill_session(s)

    def delete_ticket(self):
        if confirmations.confirm_deletion():
            s = classes.create_session(self.engine)
            d = s.query(classes.Ticket).filter_by(id=self.id).first()
            s.delete(d)
            classes.kill_session(s)
            self.accept()

    def save_changes(self):
        a = confirmations.confirm_editing()
        if not a:
            return
        n = self.voucherNumberLineEdit.text()
        payonbus = self.doubleSpinBox.value()
        s = classes.create_session(self.engine)
        tick = s.query(classes.Ticket).filter_by(id=self.id).first()
        tick.number = self.voucherNumberLineEdit.text()
        tick.customer = self.lineedit_client.text()
        tick.adults = self.adultsSpinBox.value()
        tick.children = self.childrenSpinBox.value()
        tick.infants = self.infantsSpinBox.value()
        tick.room = self.roomNumberLineEdit.text()
        tick.pay_on_bus = payonbus
        tick.price = self.ticketPriceDoubleSpinBox.value()
        tick.pickup_point_name = self.comboBox.currentText()
        tick.pickup_points_id = self.comboBox.itemData(self.comboBox.currentIndex())
        tick.comments = self.commentsTextInput.text()
        tick.date = self.sale_date_edit.date().toPython()
        tick.hotels_id = self.hotelComboBox.itemData(self.hotelComboBox.currentIndex())
        tick.trips_id = self.tripInput.itemData(self.tripInput.currentIndex())
        tick.vendors_id = self.chooseVendorComboBox.itemData(self.chooseVendorComboBox.currentIndex())
        tick.languages_id = self.languageComboBox.itemData(self.languageComboBox.currentIndex())
        classes.kill_session(s)
        self.accept()


class EditSignature(QDialog,sigchange_ui.Ui_sig_change):
    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)
        self.setModal(True)
        self.setFixedSize(self.width(), self.height())
        self.tx = ''
        try:
            with open(r'signature.txt', 'rb') as f:
                self.tx = f.read().decode('utf-8')
        except:
            self.tx = "ΛΕΥΘΕΡΙΩΤΗΣ Χ. ΣΠΥΡΙΔΩΝ\nΓΡΑΦΕΙΟ ΓΕΝΙΚΟΥ ΤΟΥΡΙΣΜΟΥ\nΣ. ΒΕΝΙΖΕΛΟΥ 21, ΡΕΘΥΜΝΟ\n"
            self.tx += "ΤΗΛ. 28310 56641\nΑΦΜ 036314510 ΔΟΥ ΡΕΘΥΜΝΟΥ"
            try:
                with open(r'signature.txt', 'wb') as f:
                    f.write(self.tx.encode('utf-8'))
            except:
                msg = QMessageBox()
                msg.setWindowTitle("Error")
                msg.setText('Error opening signature file. Check permissions.')
                msg.setIcon(QMessageBox.Critical)
                msg.exec_()
        self.sig_text.insertPlainText(self.tx)
        self.sig_buttonbox.accepted.connect(self.save)

    def save(self):
        t = self.sig_text.toPlainText()
        print(t)
        try:
            with open(r'signature.txt', 'wb') as f:
                f.write(t.encode('utf-8'))
        except:
            msg = QMessageBox()
            msg.setWindowTitle("Error")
            msg.setText('Error opening signature file. Check permissions.')
            msg.setIcon(QMessageBox.Critical)
            msg.exec_()
        self.accept()
