# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\loadingbarwidget.ui'
#
# Created: Wed May 18 17:52:01 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_LoadingBarWidget(object):
    def setupUi(self, LoadingBarWidget):
        LoadingBarWidget.setObjectName("LoadingBarWidget")
        LoadingBarWidget.resize(379, 39)
        LoadingBarWidget.setModal(True)
        self.gridLayout = QtGui.QGridLayout(LoadingBarWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.progressBar = QtGui.QProgressBar(LoadingBarWidget)
        self.progressBar.setMaximum(0)
        self.progressBar.setProperty("value", -1)
        self.progressBar.setAlignment(QtCore.Qt.AlignCenter)
        self.progressBar.setTextVisible(True)
        self.progressBar.setObjectName("progressBar")
        self.gridLayout.addWidget(self.progressBar, 0, 0, 1, 1)

        self.retranslateUi(LoadingBarWidget)
        QtCore.QMetaObject.connectSlotsByName(LoadingBarWidget)

    def retranslateUi(self, LoadingBarWidget):
        LoadingBarWidget.setWindowTitle(QtGui.QApplication.translate("LoadingBarWidget", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.progressBar.setFormat(QtGui.QApplication.translate("LoadingBarWidget", "Loading...", None, QtGui.QApplication.UnicodeUTF8))

