# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\newlangdialog.ui'
#
# Created: Sat Apr 30 03:18:26 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_new_lang_dialog(object):
    def setupUi(self, new_lang_dialog):
        new_lang_dialog.setObjectName("new_lang_dialog")
        new_lang_dialog.resize(new_lang_dialog.sizeHint().width()+30, new_lang_dialog.sizeHint().height())
        new_lang_dialog.setModal(True)
        self.formLayout = QtGui.QFormLayout(new_lang_dialog)
        self.formLayout.setObjectName("formLayout")
        self.combobox_language = QtGui.QComboBox(new_lang_dialog)
        self.combobox_language.setMaximumSize(QtCore.QSize(70, 16777215))
        self.combobox_language.setObjectName("combobox_language")
        self.formLayout.setWidget(1, QtGui.QFormLayout.SpanningRole, self.combobox_language)
        self.label_del_lang = QtGui.QLabel(new_lang_dialog)
        self.label_del_lang.setObjectName("label_del_lang")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_del_lang)
        self.button_del = QtGui.QPushButton(new_lang_dialog)
        self.button_del.setObjectName("button_del")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.button_del)
        self.label_lang_name = QtGui.QLabel(new_lang_dialog)
        self.label_lang_name.setObjectName("label_lang_name")
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.label_lang_name)
        self.lineedit_lang_name = QtGui.QLineEdit(new_lang_dialog)
        self.lineedit_lang_name.setObjectName("lineedit_lang_name")
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineedit_lang_name)
        self.buttonbox_lang = QtGui.QDialogButtonBox(new_lang_dialog)
        self.buttonbox_lang.setOrientation(QtCore.Qt.Horizontal)
        self.buttonbox_lang.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonbox_lang.setObjectName("buttonbox_lang")
        self.formLayout.setWidget(4, QtGui.QFormLayout.SpanningRole, self.buttonbox_lang)

        self.retranslateUi(new_lang_dialog)
        QtCore.QObject.connect(self.buttonbox_lang, QtCore.SIGNAL("accepted()"), new_lang_dialog.accept)
        QtCore.QObject.connect(self.buttonbox_lang, QtCore.SIGNAL("rejected()"), new_lang_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(new_lang_dialog)

    def retranslateUi(self, new_lang_dialog):
        new_lang_dialog.setWindowTitle(QtGui.QApplication.translate("new_lang_dialog", "Languages", None, QtGui.QApplication.UnicodeUTF8))
        self.label_lang_name.setText(QtGui.QApplication.translate("new_lang_dialog", "Add Language (2-3 symbols):", None, QtGui.QApplication.UnicodeUTF8))
        self.label_del_lang.setText(QtGui.QApplication.translate("label_del_lang", "Delete selected language:", None, QtGui.QApplication.UnicodeUTF8))
        self.button_del.setText(QtGui.QApplication.translate("button_del", "Delete", None, QtGui.QApplication.UnicodeUTF8))