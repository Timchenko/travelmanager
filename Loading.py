from PySide import QtCore
from PySide.QtGui import QDialog
from PySide.QtGui import QProgressBar
from loadingbarwidget_ui import Ui_LoadingBarWidget


class Loading(QDialog):
    def __init__(self):
        QDialog.__init__(self)
        self.setWid
        self.n = LoadingNew()
        self.setModal(True)
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)


class LoadingNew(QProgressBar):
    """
        Progress bar in busy mode with text displayed at the center.
    """

    def __init__(self):
        super().__init__()
        self.setRange(0, 0)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self._text = None
        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)

    def setText(self, text):
        self._text = text

    def text(self):
        return "Loading..."