# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\newpickupdialog.ui'
#
# Created: Fri May  6 17:41:45 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_newpickup_dialog(object):
    def setupUi(self, newpickup_dialog):
        newpickup_dialog.setObjectName("newpickup_dialog")
        newpickup_dialog.resize(840, 394)
        self.gridLayout = QtGui.QGridLayout(newpickup_dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.label_name = QtGui.QLabel(newpickup_dialog)
        self.label_name.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_name.setObjectName("label_name")
        self.gridLayout.addWidget(self.label_name, 0, 0, 1, 1)
        self.lineedit_name = QtGui.QLineEdit(newpickup_dialog)
        self.lineedit_name.setObjectName("lineedit_name")
        self.gridLayout.addWidget(self.lineedit_name, 0, 1, 1, 2)
        self.tableview_routes = QtGui.QTableView(newpickup_dialog)
        self.tableview_routes.setObjectName("tableview_routes")
        self.gridLayout.addWidget(self.tableview_routes, 2, 0, 2, 2)
        self.lineedit_altname = QtGui.QLineEdit(newpickup_dialog)
        self.lineedit_altname.setObjectName("lineedit_altname")
        self.gridLayout.addWidget(self.lineedit_altname, 2, 2, 1, 1)
        self.toolbutton_plus = QtGui.QToolButton(newpickup_dialog)
        self.toolbutton_plus.setObjectName("toolbutton_plus")
        self.gridLayout.addWidget(self.toolbutton_plus, 2, 3, 1, 1)
        self.listwidg_altnames = QtGui.QListWidget(newpickup_dialog)
        self.listwidg_altnames.setObjectName("listwidg_altnames")
        self.gridLayout.addWidget(self.listwidg_altnames, 3, 2, 1, 2)
        self.newpickup_buttonbox = QtGui.QDialogButtonBox(newpickup_dialog)
        self.newpickup_buttonbox.setOrientation(QtCore.Qt.Horizontal)
        self.newpickup_buttonbox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.newpickup_buttonbox.setObjectName("newpickup_buttonbox")
        self.gridLayout.addWidget(self.newpickup_buttonbox, 4, 2, 1, 2)
        self.label_altnames = QtGui.QLabel(newpickup_dialog)
        self.label_altnames.setAlignment(QtCore.Qt.AlignCenter)
        self.label_altnames.setObjectName("label_altnames")
        self.gridLayout.addWidget(self.label_altnames, 1, 2, 1, 2)
        self.label_routes_times = QtGui.QLabel(newpickup_dialog)
        self.label_routes_times.setAlignment(QtCore.Qt.AlignCenter)
        self.label_routes_times.setObjectName("label_routes_times")
        self.gridLayout.addWidget(self.label_routes_times, 1, 0, 1, 2)
        self.lineedit_altname.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        self.listwidg_altnames.setSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Expanding)
        self.retranslateUi(newpickup_dialog)
        QtCore.QObject.connect(self.newpickup_buttonbox, QtCore.SIGNAL("accepted()"), newpickup_dialog.accept)
        QtCore.QObject.connect(self.newpickup_buttonbox, QtCore.SIGNAL("rejected()"), newpickup_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(newpickup_dialog)

    def retranslateUi(self, newpickup_dialog):
        newpickup_dialog.setWindowTitle(QtGui.QApplication.translate("newpickup_dialog", "New Pickup", None, QtGui.QApplication.UnicodeUTF8))
        self.label_name.setText(QtGui.QApplication.translate("newpickup_dialog", "Name:", None, QtGui.QApplication.UnicodeUTF8))
        self.toolbutton_plus.setText(QtGui.QApplication.translate("newpickup_dialog", "+", None, QtGui.QApplication.UnicodeUTF8))
        self.label_altnames.setText(QtGui.QApplication.translate("newpickup_dialog", "Alternative names", None, QtGui.QApplication.UnicodeUTF8))
        self.label_routes_times.setText(QtGui.QApplication.translate("newpickup_dialog", "Routes and times", None, QtGui.QApplication.UnicodeUTF8))

