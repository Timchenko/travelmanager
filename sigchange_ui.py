# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\sigchange.ui'
#
# Created: Mon May 30 17:18:37 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_sig_change(object):
    def setupUi(self, sig_change):
        sig_change.setObjectName("sig_change")
        sig_change.resize(400, 145)
        self.gridLayout = QtGui.QGridLayout(sig_change)
        self.gridLayout.setObjectName("gridLayout")
        self.sig_text = QtGui.QPlainTextEdit(sig_change)
        self.sig_text.setObjectName("sig_text")
        self.gridLayout.addWidget(self.sig_text, 0, 0, 1, 1)
        self.sig_buttonbox = QtGui.QDialogButtonBox(sig_change)
        self.sig_buttonbox.setOrientation(QtCore.Qt.Horizontal)
        self.sig_buttonbox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Save)
        self.sig_buttonbox.setObjectName("sig_buttonbox")
        self.gridLayout.addWidget(self.sig_buttonbox, 1, 0, 1, 1)

        self.retranslateUi(sig_change)
        QtCore.QObject.connect(self.sig_buttonbox, QtCore.SIGNAL("accepted()"), sig_change.accept)
        QtCore.QObject.connect(self.sig_buttonbox, QtCore.SIGNAL("rejected()"), sig_change.reject)
        QtCore.QMetaObject.connectSlotsByName(sig_change)

    def retranslateUi(self, sig_change):
        sig_change.setWindowTitle(QtGui.QApplication.translate("sig_change", "Dialog", None, QtGui.QApplication.UnicodeUTF8))

