import classes
import json

class StrTrip(classes.Trip):
    def __init__(self, trip):
        classes.Trip.__init__(self)
        self.bus = trip.buses.name + ' (' + self.buses.max_passengers.__str__() + ')'
        self.name = trip.routes.name + ' / ' + self.bus


class StrTicket(classes.Ticket):
    def __init__(self, ticket):
        self.id = ticket.id
        self.name = ticket.name
        self.trip = ticket.trip.routes.name
        self.vend = self.vendors.name
        self.lang = self.languages.name
