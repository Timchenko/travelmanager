# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\newtripdialog.ui'
#
# Created: Fri May  6 17:42:53 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_trip_addition_dialog(object):
    def setupUi(self, trip_addition_dialog):
        trip_addition_dialog.setObjectName("trip_addition_dialog")
        trip_addition_dialog.resize(trip_addition_dialog.sizeHint().width(), trip_addition_dialog.sizeHint().height())
        self.gridLayout = QtGui.QGridLayout(trip_addition_dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtGui.QLabel(trip_addition_dialog)
        self.label.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.bus_combobox = QtGui.QComboBox(trip_addition_dialog)
        self.bus_combobox.setObjectName("bus_combobox")
        self.gridLayout.addWidget(self.bus_combobox, 0, 1, 1, 1)
        self.label_2 = QtGui.QLabel(trip_addition_dialog)
        self.label_2.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_2.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_2.setObjectName("label_2")
        self.gridLayout.addWidget(self.label_2, 0, 2, 1, 1)
        self.dateTimeEdit = QtGui.QDateTimeEdit(trip_addition_dialog)
        self.dateTimeEdit.setMaximumSize(QtCore.QSize(120, 16777215))
        self.dateTimeEdit.setCalendarPopup(True)
        self.dateTimeEdit.setObjectName("dateTimeEdit")
        self.gridLayout.addWidget(self.dateTimeEdit, 0, 3, 1, 1)

        self.guides_combobox = QtGui.QComboBox(trip_addition_dialog)
        self.guides_combobox.setObjectName("guides_combobox")
        self.gridLayout.addWidget(self.guides_combobox, 1, 1, 1, 1)
        self.label_guides = QtGui.QLabel(trip_addition_dialog)
        self.label_guides.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_guides.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_guides.setObjectName("label_guides")
        self.gridLayout.addWidget(self.label_guides, 1, 0, 1, 1)

        self.drivers_combobox = QtGui.QComboBox(trip_addition_dialog)
        self.drivers_combobox.setObjectName("drivers_combobox")
        self.gridLayout.addWidget(self.drivers_combobox, 2, 1, 1, 1)
        self.label_drivers = QtGui.QLabel(trip_addition_dialog)
        self.label_drivers.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_drivers.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_drivers.setObjectName("label_drivers")
        self.gridLayout.addWidget(self.label_drivers, 2, 0, 1, 1)

        self.route_combobox = QtGui.QComboBox(trip_addition_dialog)
        self.route_combobox.setObjectName("route_combobox")
        self.gridLayout.addWidget(self.route_combobox, 3, 1, 1, 3)
        self.label_3 = QtGui.QLabel(trip_addition_dialog)
        self.label_3.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_3.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 3, 0, 1, 1)
        self.trip_addition_buttonbox = QtGui.QDialogButtonBox(trip_addition_dialog)
        self.trip_addition_buttonbox.setOrientation(QtCore.Qt.Horizontal)
        self.trip_addition_buttonbox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.trip_addition_buttonbox.setObjectName("trip_addition_buttonbox")
        self.gridLayout.addWidget(self.trip_addition_buttonbox, 4, 0, 1, 4)

        self.retranslateUi(trip_addition_dialog)
        QtCore.QObject.connect(self.trip_addition_buttonbox, QtCore.SIGNAL("accepted()"), trip_addition_dialog.accept)
        QtCore.QObject.connect(self.trip_addition_buttonbox, QtCore.SIGNAL("rejected()"), trip_addition_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(trip_addition_dialog)

    def retranslateUi(self, trip_addition_dialog):
        trip_addition_dialog.setWindowTitle(QtGui.QApplication.translate("trip_addition_dialog", "New Trip", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("trip_addition_dialog", "Bus:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_guides.setText(QtGui.QApplication.translate("trip_addition_dialog", "Guide:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_drivers.setText(QtGui.QApplication.translate("trip_addition_dialog", "Driver:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("trip_addition_dialog", "Date:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("trip_addition_dialog", "Route:", None, QtGui.QApplication.UnicodeUTF8))

