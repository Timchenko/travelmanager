from PySide.QtGui import QMessageBox

def write_fin(filename, list_of_lists, totl, txt):
    if list_of_lists in [[], None]:
        return
    from openpyxl import Workbook
    from openpyxl.compat import range
    from openpyxl.cell import get_column_letter
    from openpyxl.styles import Font

    wb = Workbook()
    ws = wb.active

    ws.page_setup.orientation = ws.ORIENTATION_LANDSCAPE
    ws.page_setup.paperSize = ws.PAPERSIZE_A4
    ws.page_setup.fitToHeight = 0
    ws.page_setup.fitToWidth = 1
    vertical_shift = 3
    horisontal_shift = 1
    lenlist = len(list_of_lists[0])
    str_text = len(txt)
    for i in range(str_text):
        ws.merge_cells(start_row=vertical_shift+i,
                       end_row=vertical_shift+i,
                       start_column=horisontal_shift,
                       end_column= horisontal_shift+lenlist - 9)
        cel = ws.cell(row=vertical_shift+i, column=horisontal_shift)
        cel.value = txt[i]
    sig = []
    try:
        with open(r'signature.txt', 'rb') as f:
            sig = f.read().decode('utf-8').split('\n')
    except:
        msg = QMessageBox()
        msg.setWindowTitle("Error")
        msg.setText('Error opening signature file. Check permissions. Skipping signature for now...')
        msg.setIcon(QMessageBox.Warning)
        msg.exec_()
    str_text = len(sig)
    for i in range(str_text):
        ws.merge_cells(start_row=vertical_shift+i,
                       end_row=vertical_shift+i,
                       start_column=horisontal_shift+lenlist - 8,
                       end_column= horisontal_shift+lenlist-1)
        cel = ws.cell(row=vertical_shift+i, column=horisontal_shift+lenlist - 8)
        cel.value = sig[i]
    vertical_shift += 8


    headers = ['Rep', 'Date', 'Route', 'Voucher', 'Lang',  'AD', 'CH', 'IN', 'Price', 'PoB']
    boldfont = Font(bold=True)
    for i, d in enumerate(headers):
        c = ws.cell(row=vertical_shift, column=horisontal_shift + i)
        c.font = boldfont
        c.value = d

    vertical_shift += 1
    column_widths = []
    for i in range(len(list_of_lists)):
        for j in range(len(list_of_lists[i])):
            if len(column_widths) > j:
                if len(list_of_lists[i][j].__str__())+3 > column_widths[j]:
                    column_widths[j] = 3 + len(list_of_lists[i][j].__str__())
            else:
                column_widths.append(3 + len(list_of_lists[i][j].__str__()))
            c = ws.cell(row=i+vertical_shift, column=j+horisontal_shift)
            c.value = list_of_lists[i][j]

    c = ws.cell(row=i+vertical_shift+2, column=j+horisontal_shift-2)
    c.font = boldfont
    c.value = "Total:"
    c = ws.cell(row=i+vertical_shift+2, column=j+horisontal_shift-1)
    c.font = boldfont
    c.value = totl
    for i, column_width in enumerate(column_widths):
        ws.column_dimensions[get_column_letter(horisontal_shift+i)].width = column_width
    flag = True
    while flag:
        try:
            wb.save(filename=filename + '.xlsx')
            flag = False
        except PermissionError:
            msg = QMessageBox()
            msg.setWindowTitle("Error")
            msg.setText('Can\'t save file. Check if it\'s open in another program.')
            msg.setIcon(QMessageBox.Critical)
            okbutton = msg.addButton("Try Again", QMessageBox.YesRole)
            canc = msg.addButton("Cancel Saving", QMessageBox.NoRole)
            msg.exec_()
            if msg.clickedButton() == canc:
                flag = False



def write_util(filename, list_of_lists, cap, totl, avail, txt):
    if list_of_lists in [[], None]:
        return
    from openpyxl import Workbook
    from openpyxl.compat import range
    from openpyxl.cell import get_column_letter
    from openpyxl.styles import Font

    wb = Workbook()
    ws = wb.active

    ws.page_setup.orientation = ws.ORIENTATION_LANDSCAPE
    ws.page_setup.paperSize = ws.PAPERSIZE_A4
    ws.page_setup.fitToHeight = 0
    ws.page_setup.fitToWidth = 1
    vertical_shift = 3
    horisontal_shift = 1
    lenlist = len(list_of_lists[0])
    str_text = len(txt)
    column_widths = []
    for i in range(str_text):
        ws.merge_cells(start_row=vertical_shift+i,
                       end_row=vertical_shift+i,
                       start_column=horisontal_shift,
                       end_column= horisontal_shift+lenlist-10)
        cel = ws.cell(row=vertical_shift+i, column=horisontal_shift)
        cel.value = txt[i]

    sig = []
    try:
        with open(r'signature.txt', 'rb') as f:
            sig = f.read().decode('utf-8').split('\n')
    except:
        msg = QMessageBox()
        msg.setWindowTitle("Error")
        msg.setText('Error opening signature file. Check permissions. Skipping signature for now...')
        msg.setIcon(QMessageBox.Warning)
        msg.exec_()
    str_text = len(sig)
    for i in range(str_text):
        ws.merge_cells(start_row=vertical_shift+i,
                       end_row=vertical_shift+i,
                       start_column=horisontal_shift+lenlist - 9,
                       end_column= horisontal_shift+lenlist-1)
        cel = ws.cell(row=vertical_shift+i, column=horisontal_shift+lenlist - 9)
        cel.value = sig[i]
    vertical_shift += 6

    headers = ['Time', 'Pickup', 'Hotel', 'Room', 'Name', 'Voucher', 'AD', 'CH', 'IN', 'Lang', 'PoB', 'Rep', 'Notes']
    boldfont = Font(bold=True)
    for i, d in enumerate(headers):
        c = ws.cell(row=vertical_shift, column=horisontal_shift + i)
        c.font = boldfont
        c.value = d

    vertical_shift += 1
    for i in range(len(list_of_lists)):
        for j in range(len(list_of_lists[i])):
            if len(column_widths) > j:
                if len(list_of_lists[i][j].__str__())+3 > column_widths[j]:
                    column_widths[j] = 3+len(list_of_lists[i][j].__str__())
            else:
                column_widths.append(3+len(list_of_lists[i][j].__str__()))
            c = ws.cell(row=i+vertical_shift, column=j+horisontal_shift)
            c.value = list_of_lists[i][j]

    c = ws.cell(row=i+vertical_shift+2, column=j+horisontal_shift-7)
    c.font = boldfont
    c.value = "Total:"
    c = ws.cell(row=i+vertical_shift+2, column=j+horisontal_shift-6)
    c.font = boldfont
    c.value = cap
    c = ws.cell(row=i+vertical_shift+3, column=j+horisontal_shift-7)
    c.font = boldfont
    c.value = "Reserved:"
    c = ws.cell(row=i+vertical_shift+3, column=j+horisontal_shift-6)
    c.font = boldfont
    c.value = totl
    c = ws.cell(row=i+vertical_shift+4, column=j+horisontal_shift-7)
    c.font = boldfont
    c.value = "Available:"
    c = ws.cell(row=i+vertical_shift+4, column=j+horisontal_shift-6)
    c.font = boldfont
    c.value = avail
    for i, column_width in enumerate(column_widths):
        ws.column_dimensions[get_column_letter(horisontal_shift+i)].width = column_width

    flag = True
    while flag:
        try:
            wb.save(filename=filename + '.xlsx')
            flag = False
        except PermissionError:
            msg = QMessageBox()
            msg.setWindowTitle("Error")
            msg.setText('Can\'t save file. Check if it\'s open in another program.')
            msg.setIcon(QMessageBox.Critical)
            okbutton = msg.addButton("Try Again", QMessageBox.YesRole)
            canc = msg.addButton("Cancel Saving", QMessageBox.NoRole)
            msg.exec_()
            if msg.clickedButton() == canc:
                flag = False