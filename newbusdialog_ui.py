# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\newbusdialog.ui'
#
# Created: Sat Apr 30 03:18:26 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_new_bus_dialog(object):
    def setupUi(self, new_bus_dialog):
        new_bus_dialog.setObjectName("new_bus_dialog")
        new_bus_dialog.resize(new_bus_dialog.sizeHint().width(), new_bus_dialog.sizeHint().height())
        new_bus_dialog.setModal(True)
        self.formLayout = QtGui.QFormLayout(new_bus_dialog)
        self.formLayout.setObjectName("formLayout")
        self.label_bus_name = QtGui.QLabel(new_bus_dialog)
        self.label_bus_name.setObjectName("label_bus_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_bus_name)
        self.lineedit_bus_name = QtGui.QLineEdit(new_bus_dialog)
        self.lineedit_bus_name.setObjectName("lineedit_bus_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineedit_bus_name)
        self.label_bus_capacity = QtGui.QLabel(new_bus_dialog)
        self.label_bus_capacity.setObjectName("label_bus_capacity")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_bus_capacity)
        self.lineedit_bus_capacity = QtGui.QSpinBox(new_bus_dialog)
        self.lineedit_bus_capacity.setObjectName("lineedit_bus_capacity")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineedit_bus_capacity)
        self.buttonbox_bus = QtGui.QDialogButtonBox(new_bus_dialog)
        self.buttonbox_bus.setOrientation(QtCore.Qt.Horizontal)
        self.buttonbox_bus.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonbox_bus.setObjectName("buttonbox_bus")
        self.formLayout.setWidget(3, QtGui.QFormLayout.SpanningRole, self.buttonbox_bus)

        self.retranslateUi(new_bus_dialog)
        QtCore.QObject.connect(self.buttonbox_bus, QtCore.SIGNAL("accepted()"), new_bus_dialog.accept)
        QtCore.QObject.connect(self.buttonbox_bus, QtCore.SIGNAL("rejected()"), new_bus_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(new_bus_dialog)

    def retranslateUi(self, new_bus_dialog):
        new_bus_dialog.setWindowTitle(QtGui.QApplication.translate("new_bus_dialog", "Add Bus", None, QtGui.QApplication.UnicodeUTF8))
        self.label_bus_name.setText(QtGui.QApplication.translate("new_bus_dialog", "Name:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_bus_capacity.setText(QtGui.QApplication.translate("new_bus_dialog", "Capacity:", None, QtGui.QApplication.UnicodeUTF8))

