# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\newroutedialog.ui'
#
# Created: Fri May  6 17:41:34 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_newroute_dialog(object):
    def setupUi(self, newroute_dialog):
        newroute_dialog.setObjectName("newroute_dialog")
        newroute_dialog.resize(540, 394)
        self.gridLayout = QtGui.QGridLayout(newroute_dialog)
        self.gridLayout.setObjectName("gridLayout")

        self.label_name = QtGui.QLabel(newroute_dialog)
        self.label_name.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.label_name.setObjectName("label_name")
        self.gridLayout.addWidget(self.label_name, 0, 0, 1, 1)

        self.lineedit_name = QtGui.QLineEdit(newroute_dialog)
        self.lineedit_name.setObjectName("lineedit_name")
        self.gridLayout.addWidget(self.lineedit_name, 0, 1, 1, 1)

        self.label_time = QtGui.QLabel(newroute_dialog)
        self.label_time.setAlignment(QtCore.Qt.AlignCenter)
        self.label_time.setObjectName("label_time")
        self.gridLayout.addWidget(self.label_time, 0, 2, 1, 1)

        self.timeEdit = QtGui.QTimeEdit(newroute_dialog)
        self.timeEdit.setObjectName("timeEdit")
        self.gridLayout.addWidget(self.timeEdit, 0, 3, 1, 1)

        self.label_price = QtGui.QLabel(newroute_dialog)
        self.label_price.setAlignment(QtCore.Qt.AlignCenter)
        self.label_price.setObjectName("label_price")
        self.gridLayout.addWidget(self.label_price, 1, 2, 1, 1)


        self.priceEdit = QtGui.QDoubleSpinBox(newroute_dialog)
        self.priceEdit.setObjectName("priceEdit")
        self.gridLayout.addWidget(self.priceEdit, 1, 3, 1, 1)

        self.pickup_times_tableview = QtGui.QTableView(newroute_dialog)
        self.pickup_times_tableview.setObjectName("pickup_times_tableview")
        self.gridLayout.addWidget(self.pickup_times_tableview, 3, 0, 1, 4)

        self.newroute_buttonbox = QtGui.QDialogButtonBox(newroute_dialog)
        self.newroute_buttonbox.setOrientation(QtCore.Qt.Horizontal)
        self.newroute_buttonbox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.newroute_buttonbox.setObjectName("newroute_buttonbox")
        self.gridLayout.addWidget(self.newroute_buttonbox, 4, 1, 1, 3)

        self.label_pickup_times = QtGui.QLabel(newroute_dialog)
        self.label_pickup_times.setAlignment(QtCore.Qt.AlignCenter)
        self.label_pickup_times.setObjectName("label_pickup_times")
        self.gridLayout.addWidget(self.label_pickup_times, 2, 0, 1, 4)

        self.retranslateUi(newroute_dialog)
        QtCore.QObject.connect(self.newroute_buttonbox, QtCore.SIGNAL("accepted()"), newroute_dialog.accept)
        QtCore.QObject.connect(self.newroute_buttonbox, QtCore.SIGNAL("rejected()"), newroute_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(newroute_dialog)

    def retranslateUi(self, newroute_dialog):
        newroute_dialog.setWindowTitle(QtGui.QApplication.translate("newroute_dialog", "New Route", None, QtGui.QApplication.UnicodeUTF8))
        self.label_name.setText(QtGui.QApplication.translate("newroute_dialog", "Name:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_time.setText(QtGui.QApplication.translate("newroute_dialog", "Time:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_price.setText(QtGui.QApplication.translate("newroute_dialog", "Price:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_pickup_times.setText(QtGui.QApplication.translate("newroute_dialog", "Pickup times", None, QtGui.QApplication.UnicodeUTF8))

