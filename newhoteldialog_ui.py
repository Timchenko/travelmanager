# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\newhoteldialog.ui'
#
# Created: Sat Apr 30 03:18:26 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_new_hotel_dialog(object):
    def setupUi(self, new_hotel_dialog):
        new_hotel_dialog.setObjectName("new_hotel_dialog")
        new_hotel_dialog.resize(new_hotel_dialog.sizeHint().width(), new_hotel_dialog.sizeHint().height())
        new_hotel_dialog.setModal(True)
        self.formLayout = QtGui.QFormLayout(new_hotel_dialog)
        self.formLayout.setObjectName("formLayout")
        self.label_hotel_name = QtGui.QLabel(new_hotel_dialog)
        self.label_hotel_name.setObjectName("label_hotel_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_hotel_name)
        self.lineedit_hotel_name = QtGui.QLineEdit(new_hotel_dialog)
        self.lineedit_hotel_name.setObjectName("lineedit_hotel_name")
        self.lineedit_hotel_name.setMinimumWidth(200)
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineedit_hotel_name)
        self.label_pickup = QtGui.QLabel(new_hotel_dialog)
        self.label_pickup.setObjectName("label_pickup")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_pickup)
        self.combobox_pickups = QtGui.QComboBox(new_hotel_dialog)
        self.combobox_pickups.setObjectName("combobox_pickups")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.combobox_pickups)
        self.label_hotel_comments = QtGui.QLabel(new_hotel_dialog)
        self.label_hotel_comments.setObjectName("label_hotel_comments")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_hotel_comments)
        self.lineedit_hotel_comments = QtGui.QLineEdit(new_hotel_dialog)
        self.lineedit_hotel_comments.setObjectName("lineedit_hotel_comments")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineedit_hotel_comments)
        self.buttonbox_hotel = QtGui.QDialogButtonBox(new_hotel_dialog)
        self.buttonbox_hotel.setOrientation(QtCore.Qt.Horizontal)
        self.buttonbox_hotel.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonbox_hotel.setObjectName("buttonbox_hotel")
        self.formLayout.setWidget(3, QtGui.QFormLayout.SpanningRole, self.buttonbox_hotel)

        self.retranslateUi(new_hotel_dialog)
        QtCore.QObject.connect(self.buttonbox_hotel, QtCore.SIGNAL("accepted()"), new_hotel_dialog.accept)
        QtCore.QObject.connect(self.buttonbox_hotel, QtCore.SIGNAL("rejected()"), new_hotel_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(new_hotel_dialog)

    def retranslateUi(self, new_hotel_dialog):
        new_hotel_dialog.setWindowTitle(QtGui.QApplication.translate("new_hotel_dialog", "New Hotel", None, QtGui.QApplication.UnicodeUTF8))
        self.label_hotel_name.setText(QtGui.QApplication.translate("new_hotel_dialog", "Name:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_pickup.setText(QtGui.QApplication.translate("new_hotel_dialog", "Pickup:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_hotel_comments.setText(QtGui.QApplication.translate("new_hotel_dialog", "Comments:", None, QtGui.QApplication.UnicodeUTF8))

