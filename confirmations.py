from PySide.QtGui import *
import sys
import json


def confirm_deletion():
    msgBox = QMessageBox()
    msgBox.setWindowTitle("Confirm Deletion")
    msgBox.setText("You are going to delete this item.")
    msgBox.setInformativeText("This action is irreversible. Are you sure?")
    msgBox.setIcon(QMessageBox.Warning)
    del_button = msgBox.addButton("Delete", QMessageBox.YesRole)
    cancel_button = msgBox.addButton("Cancel", QMessageBox.NoRole)
    msgBox.exec_()
    if msgBox.clickedButton() == del_button:
        return True
    return False


def confirm_trip_deletion():
    msgBox = QMessageBox()
    msgBox.setWindowTitle("Confirm Deletion")
    msgBox.setText("You are going to delete this trip.")
    msgBox.setInformativeText("THIS WILL ALSO DELETE ALL THE ASSOCIATED TICKETS. This action is irreversible. Are you sure?")
    msgBox.setIcon(QMessageBox.Warning)
    del_button = msgBox.addButton("Delete", QMessageBox.YesRole)
    cancel_button = msgBox.addButton("Cancel", QMessageBox.NoRole)
    msgBox.exec_()
    if msgBox.clickedButton() == del_button:
        return True
    return False


def confirm_editing():
    msgBox = QMessageBox()
    msgBox.setWindowTitle("Confirm Changes")
    msgBox.setText("You are going to apply changes to this item.")
    msgBox.setInformativeText("This action is irreversible. Are you sure?")
    msgBox.setIcon(QMessageBox.Information)
    save_button = msgBox.addButton("Save", QMessageBox.YesRole)
    cancel_button = msgBox.addButton("Cancel", QMessageBox.NoRole)
    msgBox.exec_()
    if msgBox.clickedButton() == save_button:
        return True
    return False


if __name__ == "__main__":
    app = QApplication(sys.argv)
    dialog = QDialog()
    n = confirm_deletion()
    print(n.__str__())
    dialog.show()

# class ConfirmDeletion(QMessageBox):
#     def __init__(self):
#         #QMessageBox.__init__(self, self.Warning, "Confirm Deletion", "You are going to delete this item.")
#         self.setInformativeText("This action is irreversible. Are you sure?")
#         canc = self.addButton("Cancel", self.RejectRole)
#         delete = self.addButton("Delete", self.DestructiveRole)
#         self.setDefaultButton(canc)
#         canc.clicked.connect(self.reject())
#         delete.clicked.connect(self.accept())
#         QMessageBox().__init__