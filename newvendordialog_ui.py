# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\newvendordialog.ui'
#
# Created: Sat Apr 30 03:18:58 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_new_vendor_dialog(object):
    def setupUi(self, new_vendor_dialog):
        new_vendor_dialog.setObjectName("new_vendor_dialog")
        new_vendor_dialog.resize(new_vendor_dialog.sizeHint().width(), new_vendor_dialog.sizeHint().height())
        new_vendor_dialog.setModal(True)
        self.formLayout = QtGui.QFormLayout(new_vendor_dialog)
        self.formLayout.setObjectName("formLayout")
        self.label_vendor_name = QtGui.QLabel(new_vendor_dialog)
        self.label_vendor_name.setObjectName("label_vendor_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_vendor_name)
        self.lineedit_vendor_name = QtGui.QLineEdit(new_vendor_dialog)
        self.lineedit_vendor_name.setObjectName("lineedit_vendor_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineedit_vendor_name)
        self.buttonbox_vendor = QtGui.QDialogButtonBox(new_vendor_dialog)
        self.buttonbox_vendor.setOrientation(QtCore.Qt.Horizontal)
        self.buttonbox_vendor.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonbox_vendor.setObjectName("buttonbox_vendor")
        self.formLayout.setWidget(7, QtGui.QFormLayout.SpanningRole, self.buttonbox_vendor)
        self.lineedit_vendor_afm = QtGui.QLineEdit(new_vendor_dialog)
        self.lineedit_vendor_afm.setObjectName("lineedit_vendor_afm")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineedit_vendor_afm)
        self.label_vendor_afm = QtGui.QLabel(new_vendor_dialog)
        self.label_vendor_afm.setObjectName("label_vendor_afm")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_vendor_afm)
        self.linenedit_vendor_phone = QtGui.QLineEdit(new_vendor_dialog)
        self.linenedit_vendor_phone.setObjectName("linenedit_vendor_phone")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.linenedit_vendor_phone)
        self.label_vendor_phone = QtGui.QLabel(new_vendor_dialog)
        self.label_vendor_phone.setObjectName("label_vendor_phone")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_vendor_phone)
        self.lineedit_vendor_fax = QtGui.QLineEdit(new_vendor_dialog)
        self.lineedit_vendor_fax.setObjectName("lineedit_vendor_fax")
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.lineedit_vendor_fax)
        self.label_vendor_fax = QtGui.QLabel(new_vendor_dialog)
        self.label_vendor_fax.setObjectName("label_vendor_fax")
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.label_vendor_fax)
        self.lineedit_vendor_email = QtGui.QLineEdit(new_vendor_dialog)
        self.lineedit_vendor_email.setObjectName("lineedit_vendor_email")
        self.formLayout.setWidget(4, QtGui.QFormLayout.FieldRole, self.lineedit_vendor_email)
        self.label_vendor_email = QtGui.QLabel(new_vendor_dialog)
        self.label_vendor_email.setObjectName("label_vendor_email")
        self.formLayout.setWidget(4, QtGui.QFormLayout.LabelRole, self.label_vendor_email)

        self.retranslateUi(new_vendor_dialog)
        QtCore.QObject.connect(self.buttonbox_vendor, QtCore.SIGNAL("accepted()"), new_vendor_dialog.accept)
        QtCore.QObject.connect(self.buttonbox_vendor, QtCore.SIGNAL("rejected()"), new_vendor_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(new_vendor_dialog)

    def retranslateUi(self, new_vendor_dialog):
        new_vendor_dialog.setWindowTitle(QtGui.QApplication.translate("new_vendor_dialog", "Add Rep", None, QtGui.QApplication.UnicodeUTF8))
        self.label_vendor_name.setText(QtGui.QApplication.translate("new_vendor_dialog", "Name:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_vendor_afm.setText(QtGui.QApplication.translate("new_vendor_dialog", "AFM:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_vendor_phone.setText(QtGui.QApplication.translate("new_vendor_dialog", "Phone:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_vendor_fax.setText(QtGui.QApplication.translate("new_vendor_dialog", "Fax:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_vendor_email.setText(QtGui.QApplication.translate("new_vendor_dialog", "E-mail:", None, QtGui.QApplication.UnicodeUTF8))

