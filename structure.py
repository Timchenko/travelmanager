from PySide.QtCore import *
from PySide.QtGui import *
import sys
import pdf
import json
import datetime
import travelmainwindow_ui
import classes
import addition_classes
import tables
import export_excel


class TravManMain(QMainWindow, travelmainwindow_ui.Ui_MainWindow):
    def __init__(self):
        splash.showMessage("Initializing main GUI...")
        QMainWindow.__init__(self)
        self.statusBar().showMessage('LOADING...')
        self.setupUi(self)
        splash.showMessage("Main GUI OK. Connecting to database...")

        self.engine = None
        try:
            self.engine = classes.init_db()
            if self.engine is None:
                raise Exception
        except:
            import traceback
            log_result = '\n\n---------- ' + datetime.now().__str__() + ' ----------\n'
            log_result += traceback.format_exc() + '\n' + '-'*48 + '\n'
            with open(r'errorlog.txt', 'ab') as infile:
                infile.write(log_result.encode('utf-8'))
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle("Error")
            msg.setText("Error connecting to the database. Error logged.")
            msg.exec_()
        splash.showMessage("Databse OK. Generating GUI...")
        database_options = QAction('&Database', self)
        database_options.setShortcut('Ctrl+O')
        database_options.setStatusTip('Database connection options.')
        database_options.triggered.connect(self.databaseOptions)

        signature = QAction('Signature (&I)', self)
        signature.setShortcut('Ctrl+I')
        signature.setStatusTip('Change signature.')
        signature.triggered.connect(self.change_sig)

        language_addition = QAction('&Add language', self)
        language_addition.setStatusTip('Add another language.')
        language_addition.triggered.connect(self.add_language)

        menubar = self.menuBar()
        options_menu = menubar.addMenu('&Options')
        options_menu.addAction(language_addition)
        options_menu.addAction(database_options)
        options_menu.addAction(signature)
        splash.showMessage("Main GUI generated. Fetching data...")

        # Initializing prices
        self.pricePerCapita = 0
        self.totalPrice = 0




        self.util_date_chooser.setDate(QDate.currentDate())
        self.fin_date_edit_from.setDate(QDate.currentDate().addDays(-30))
        self.fin_date_edit_to.setDate(QDate.currentDate())
        self.trips_date_from.setDate(QDate.currentDate().addDays(-1))
        self.trips_date_to.setDate(QDate.currentDate().addDays(7))

        self.reset_ticket()
        self.payOnBusAmountDoubleSpinBox.setMaximum(999999)
        """
        self.util_passenger_table
        """
        splash.showMessage("Connecting buttons...")
        self.button_save.clicked.connect(self.saveAction)
        self.tripInput.currentIndexChanged.connect(self.routeChanged)
        self.adultsSpinBox.valueChanged.connect(self.peopleChanged)
        self.childrenSpinBox.valueChanged.connect(self.peopleChanged)
        self.hotelComboBox.currentIndexChanged.connect(self.hotelChanged)

        self.payOnBusCheckBox.stateChanged.connect(self.payOnBusChanged)
        self.not_sold_today_chk.stateChanged.connect(self.not_today_changed)

        self.dateOfTheTrip.dateChanged.connect(self.date_changed)

        self.trips_apply_button.clicked.connect(self.activate_trips_table)

        self.fin_refresh_button.clicked.connect(self.refresh_fin)
        self.util_refresh_button.clicked.connect(self.refresh_utils)

        self.button_save.setStyleSheet("background-color: lightgreen")
        self.fin_refresh_button.setStyleSheet("background-color: lightgreen")
        self.util_refresh_button.setStyleSheet("background-color: lightgreen")
        self.trips_apply_button.setStyleSheet("background-color: lightgreen")

        self.add_driver_button.clicked.connect(self.add_new_driver)
        self.add_guide_button.clicked.connect(self.add_new_guide)
        self.add_vendor_button.clicked.connect(self.add_new_vendor)
        self.add_bus_button.clicked.connect(self.add_new_bus)
        self.add_hotel_button.clicked.connect(self.add_new_hotel)
        self.add_route_button.clicked.connect(self.add_new_route)
        self.add_trip_button.clicked.connect(self.add_new_trip)
        self.add_pickup_button.clicked.connect(self.add_pickup_point)

        self.fin_button_export_pdf.clicked.connect(self.export_fin_pdf)
        self.util_button_export_pdf.clicked.connect(self.export_util_pdf)

        self.fin_button_export_xls.clicked.connect(self.export_fin_xls)
        self.util_button_export_xls.clicked.connect(self.export_util_xls)

        self.buses_table.doubleClicked.connect(self.edit_bus)
        self.trips_table.doubleClicked.connect(self.edit_trip)
        self.hotel_table.doubleClicked.connect(self.edit_hotel)
        self.guides_table.doubleClicked.connect(self.edit_guide)
        self.drivers_table.doubleClicked.connect(self.edit_driver)
        self.pickup_table.doubleClicked.connect(self.edit_pickup)
        self.routes_table.doubleClicked.connect(self.edit_route)
        self.vendors_table.doubleClicked.connect(self.edit_vendor)
        self.util_passenger_table.doubleClicked.connect(self.util_edit_ticket)
        self.fin_ticket_table.doubleClicked.connect(self.fin_edit_ticket)

        self.tabWidget.currentChanged.connect(self.tab_switch)
        self.ticket_list_widget.currentChanged.connect(self.tab_switch_tickets)
        self.additionTabWidget.currentChanged.connect(self.tab_switch_addition)
        self.statusBar().showMessage('Ready.')

    def saveAction(self):
        self.statusBar().showMessage('Saving ticket. Please wait...')
        if (self.lineedit_client.text() == "" or
                    (self.childrenSpinBox.value() + self.adultsSpinBox.value()) <= 0 or
                    self.chooseVendorComboBox.currentIndex() == -1 or
                    self.tripInput.currentIndex() == -1 or
                    self.pickup_point_combobox.currentIndex() == -1 or
                    self.voucherNumberLineEdit == ""):
            msg = QMessageBox()
            msg.setText("Fill all the necessary fields!")
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle("Saving failed")
            msg.exec_()
            self.statusBar().showMessage('Ready.')
            return

        n = self.voucherNumberLineEdit.text()
        if (n != ""):
            session = classes.create_session(self.engine)
            t = session.query(classes.Ticket).filter_by(number=n).first()
            if t != None:
                msg = QMessageBox()
                msg.setText("Voucher with number {0} already exists. Client {1} goes to {2} on {3}.".format(n,
                                                                                                   t.customer,
                                                                                                   t.trips.routes.name,
                                                                                                   t.trips.start_date_time.strftime("%Y.%m.%d")))
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("Saving failed")
                msg.exec_()
                classes.kill_session(session)
                self.statusBar().showMessage('Ready.')
                return
            classes.kill_session(session)
        u = self.hotelComboBox.itemData(self.hotelComboBox.currentIndex())
        if u is None:
            session = classes.create_session(self.engine)
            n = self.hotelComboBox.currentText()
            if n == "":
                classes.kill_session(session)
                msg = QMessageBox()
                msg.setText("You haven't given the name of the hotel. If there is no name, try descriptive term, like 'Apartment'.")
                msg.setWindowTitle("Empty hotel name")
                msg.setIcon(QMessageBox.Critical)
                msg.exec_()
                self.statusBar().showMessage('Ready.')
                return
            puid = self.pickup_point_combobox.itemData(self.pickup_point_combobox.currentIndex())
            h = classes.Hotel(name=n, pickup_points_id=puid)
            session.add(h)
            session.commit()
            u = h.id
            classes.kill_session(session)
        payonbus = 0
        if self.payOnBusCheckBox.isChecked():
            payonbus = self.payOnBusAmountDoubleSpinBox.value()
        tick = classes.Ticket(number=self.voucherNumberLineEdit.text(),
                              customer=self.lineedit_client.text(),
                              adults=self.adultsSpinBox.value(),
                              children=self.childrenSpinBox.value(),
                              infants=self.infantsSpinBox.value(),
                              room=self.roomNumberLineEdit.text(),
                              pay_on_bus=payonbus,
                              price=self.ticketPriceDoubleSpinBox.value(),
                              pickup_point_name=self.pickup_point_combobox.currentText(),
                              pickup_points_id=self.pickup_point_combobox.itemData(self.pickup_point_combobox.currentIndex()),
                              comments=self.commentsTextInput.text(),
                              date=self.sale_date_edit.date().toPython(),
                              hotels_id=u,
                              trips_id=self.tripInput.itemData(self.tripInput.currentIndex()),
                              vendors_id=self.chooseVendorComboBox.itemData(self.chooseVendorComboBox.currentIndex()),
                              languages_id=self.languageComboBox.itemData(self.languageComboBox.currentIndex()))
        session = classes.create_session(self.engine)
        session.add(tick)
        classes.kill_session(session)
        self.statusBar().showMessage('Ready.')
        self.reset_ticket()

    def util_edit_ticket(self, index):
        k = self.util_ticket_model.get_id(index)
        result = addition_classes.EditTicket(self.engine, k)
        code = result.exec_()
        if code == QDialog.Accepted:
            self.refresh_utils()

    def fin_edit_ticket(self, index):
        k = self.fin_ticket_model.get_id(index)
        result = addition_classes.EditTicket(self.engine, k)
        code = result.exec_()
        if code == QDialog.Accepted:
            self.refresh_fin()

    def export_fin_pdf(self):
        self.statusBar().showMessage('Exporting financial data: PDF. Please wait...')
        l = self.fin_ticket_model.datalist
        n = []
        totl=0
        for i in l:
            row = []
            row.append(i[1])
            row.append(i[2].toString("dd/MM/yyyy"))
            row.extend(i[3:])
            n.append(row)
            totl += i[-2]

        text = ["Financial report for "]
        d = ""
        vid = self.fin_choose_vendor_combobox.itemData(self.fin_choose_vendor_combobox.currentIndex())
        if vid == None:
            d = "all reps."
        else:
            s = classes.create_session(self.engine)
            v = s.query(classes.Vendor).filter_by(id=vid).first()
            d = v.name
            text.extend(['AFM: ' + v.afm, 'Phone: ' + v.phone, 'Fax: ' + v.fax, 'E-Mail: ' + v.email])
            classes.kill_session(s)
        text[0] += d
        d1 = self.fin_date_edit_from.date().toString("dd MMM yyyy")
        d2 = self.fin_date_edit_to.date().toString("dd MMM yyyy")
        text.reverse()
        text.append("Dates: " + d1 + " through " + d2)
        text.reverse()
        fil, filt = QFileDialog.getSaveFileName(self, "Save PDF", ".\\", "PDF files (*.pdf)" )
        if not fil:
            return
        if fil[-4] =='.':
            f = fil[:-4]
        else:
            f = fil
        pdf.pdf_finance(f, n, totl.__str__(), text)
        self.statusBar().showMessage('Ready.')

    def export_util_pdf(self):
        self.statusBar().showMessage('Exporting trip data: PDF. Please wait...')
        l = self.util_ticket_model.datalist
        n = []
        totl = 0
        for i in l:
            row = []
            row.append(i[1].toString("h:mm"))
            row.extend(i[2:])
            n.append(row)
            totl += i[7] + i[8]
        text = []
        text.append(self.util_date_chooser.date().toString("dd MMM yyyy") +
                    "   Route: " +
                    self.util_combobox_trip.currentText())
        tripid = self.util_combobox_trip.itemData(self.util_combobox_trip.currentIndex())
        s = classes.create_session(self.engine)
        t = s.query(classes.Trip).filter_by(id=tripid).first()
        text.append("Driver: " + t.drivers.name + " :: " + t.drivers.phone)
        text.append("Guide: " + t.guides.name + " :: " + t.guides.phone)
        classes.kill_session(s)
        fil, filt = QFileDialog.getSaveFileName(self, "Save PDF", ".\\", "PDF files (*.pdf)" )
        if not fil:
            return
        if fil[-4] =='.':
            f = fil[:-4]
        else:
            f = fil
        pdf.pdf_utility(f, n, text)
        self.statusBar().showMessage('Ready.')

    def export_fin_xls(self):
        self.statusBar().showMessage('Exporting financial data: XLSX. Please wait...')
        l = self.fin_ticket_model.datalist
        n = []
        totl=0
        for i in l:
            row = []
            row.append(i[1])
            row.append(i[2].toString("dd/MM/yyyy"))
            row.extend(i[3:])
            n.append(row)
            totl += i[-2]

        text = ["Financial report for "]
        d = ""
        vid = self.fin_choose_vendor_combobox.itemData(self.fin_choose_vendor_combobox.currentIndex())
        lid = self.fin_choose_lang_combobox.itemData(self.fin_choose_lang_combobox.currentIndex())
        rid = self.fin_choose_route_combobox.itemData(self.fin_choose_route_combobox.currentIndex())
        if vid == None:
            d = "all reps."
        else:
            s = classes.create_session(self.engine)
            v = s.query(classes.Vendor).filter_by(id=vid).first()
            d = v.name
            text.extend(['AFM: ' + v.afm, 'Phone: ' + v.phone, 'Fax: ' + v.fax, 'E-Mail: ' + v.email])
            classes.kill_session(s)
        if lid != None:
            s = classes.create_session(self.engine)
            v = s.query(classes.Language).filter_by(id=lid).first()
            tmp = v.name
            text.extend(['Only for languages: ' + tmp + '.'])
            classes.kill_session(s)
        if rid != None:
            s = classes.create_session(self.engine)
            v = s.query(classes.Route).filter_by(id=rid).first()
            tmp = v.name
            text.extend(['Only for ' + tmp + '.'])
            classes.kill_session(s)
        text[0] += d
        d1 = self.fin_date_edit_from.date().toString("dd MMM yyyy")
        d2 = self.fin_date_edit_to.date().toString("dd MMM yyyy")
        text.reverse()
        text.append("Dates: " + d1 + " through " + d2)
        text.reverse()
        fil, filt = QFileDialog.getSaveFileName(self, "Save XLSX", ".\\", "Excel 2010 files (*.xlsx)" )
        if not fil:
            return
        if fil[-5] =='.':
            f = fil[:-5]
        else:
            f = fil
        export_excel.write_fin(f, n, totl, text)
        self.statusBar().showMessage('Ready.')

    def export_util_xls(self):
        self.statusBar().showMessage('Exporting trip data: XLSX. Please wait...')
        l = self.util_ticket_model.datalist
        n = []
        totl = 0
        cap = 0
        for i in l:
            row = []
            row.append(i[1].toString("h:mm"))
            row.extend(i[2:])
            n.append(row)
            totl += i[7] + i[8]
        text = []
        text.append(self.util_date_chooser.date().toString("dd MMM yyyy") +
                    "   Route: " +
                    self.util_combobox_trip.currentText())
        tripid = self.util_combobox_trip.itemData(self.util_combobox_trip.currentIndex())
        s = classes.create_session(self.engine)
        t = s.query(classes.Trip).filter_by(id=tripid).first()
        text.append("Driver: " + t.drivers.name + " :: " + t.drivers.phone)
        text.append("Guide: " + t.guides.name + " :: " + t.guides.phone)
        cap = t.buses.capacity
        classes.kill_session(s)
        fil, filt = QFileDialog.getSaveFileName(self, "Save XLSX", ".\\", "Excel 2010 files (*.XLSX)" )
        if not fil:
            return
        if fil[-5] =='.':
            f = fil[:-5]
        else:
            f = fil
        export_excel.write_util(f, n, cap, totl, cap - totl, text)
        self.statusBar().showMessage('Ready.')

    def tab_switch_addition(self, ind=int):
        l = ["guides",
             "drivers",
             "buses",
             "routes",
             "pickuppoints",
             "hotels",
             "vendors"]
        n = getattr(self, "activate_" + l[ind] + "_table")
        n()

    def tab_switch_tickets(self, ind=int):
        l = ["util",
             "fin"]
        n = getattr(self, "activate_" + l[ind] + "_table")
        n()

    def tab_switch(self, ind=int):
        l = ["addticket",
             "ticketlist",
             "trips",
             "addition"]
        if l[ind] == "addticket":
            self.reset_ticket()
        elif l[ind] == "ticketlist":
            n = self.ticket_list_widget.currentIndex()
            self.tab_switch_tickets(n)
        elif l[ind] == "addition":
            n = self.additionTabWidget.currentIndex()
            self.tab_switch_addition(n)
        # else:
        #     n = getattr(self, "activate_" + l[ind] + "_table")
        #     n()

    def routeChanged(self):
        if self.hotelComboBox.currentIndex() != -1:
            self.hotelChanged()
        elif self.pickup_point_combobox.currentIndex() != -1:
            self.pickup_point_changed()
        self.peopleChanged()

    def edit_bus(self, n):
        thebus = self.buses_model.get_id(n.row())
        result = addition_classes.EditBus(self.engine, thebus)
        r = result.exec_()
        if r == QDialog.Accepted:
            self.activate_buses_table()

    def edit_vendor(self, n):
        thevendor = self.vendors_model.get_id(n.row())
        result = addition_classes.EditVendor(self.engine, thevendor)
        r = result.exec_()
        if r == QDialog.Accepted:
            self.activate_vendors_table()

    def edit_hotel(self, n):
        thehotel = self.hotel_model.get_id(n.row())
        result = addition_classes.EditHotel(self.engine, thehotel)
        r = result.exec_()
        if r == QDialog.Accepted:
            self.activate_hotels_table()

    def edit_guide(self, n):
        theguide = self.guides_model.get_id(n.row())
        result = addition_classes.EditGuide(self.engine, theguide)
        r = result.exec_()
        if r == QDialog.Accepted:
            self.activate_guides_table()

    def edit_driver(self, n):
        thedriver = self.drivers_model.get_id(n.row())
        result = addition_classes.EditDriver(self.engine, thedriver)
        r = result.exec_()
        if r == QDialog.Accepted:
            self.activate_drivers_table()

    def edit_pickup(self, n):
        thepickup = self.pickup_model.get_id(n.row())
        result = addition_classes.EditPickup(self.engine, thepickup)
        r = result.exec_()
        if r == QDialog.Accepted:
            self.activate_pickuppoints_table()

    def edit_route(self, n):
        theroute = self.routes_model.get_id(n.row())
        result = addition_classes.EditRoute(self.engine, theroute)
        r = result.exec_()
        if r == QDialog.Accepted:
            self.activate_routes_table()

    def edit_trip(self, n):
        thetrip = self.trips_model.get_id(n)
        result = addition_classes.EditTrip(self.engine, thetrip)
        r = result.exec_()
        if r == QDialog.Accepted:
            self.activate_trips_table()

    def activate_hotels_table(self):
        self.statusBar().showMessage('Fetching all the hotels. Please wait...')
        sess = classes.create_session(self.engine)
        r = sess.query(classes.Hotel).all()
        try:
            self.hotel_model = tables.SpecialTable(r, parent=self.hotel_table)
        except:
            import traceback
            log_result = '\n\n---------- ' + datetime.now().__str__() + ' ----------\n'
            log_result += traceback.format_exc() + '\n' + '-'*48 + '\n'
            with open(r'errorlog.txt', 'ab') as infile:
                infile.write(log_result.encode('utf-8'))
            return
        self.hotel_table.setModel(self.hotel_model)
        font = QFont("Verdana", 9)
        self.hotel_table.setFont(font)
        self.hotel_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.hotel_table.resizeColumnsToContents()
        self.hotel_table.setSortingEnabled(True)
        self.statusBar().showMessage('Ready.')
        return

    def activate_pickuppoints_table(self):
        self.statusBar().showMessage('Retrieving pick-up points. Please wait...')
        sess = classes.create_session(self.engine)
        r = sess.query(classes.Pickup_Point).all()
        try:
            self.pickup_model = tables.SpecialTable(r, parent=self.pickup_table)
        except:
            import traceback
            log_result = '\n\n---------- ' + datetime.now().__str__() + ' ----------\n'
            log_result += traceback.format_exc() + '\n' + '-'*48 + '\n'
            with open(r'errorlog.txt', 'ab') as infile:
                infile.write(log_result.encode('utf-8'))
            return
        self.pickup_table.setModel(self.pickup_model)
        font = QFont("Verdana", 9)
        self.pickup_table.setFont(font)
        self.pickup_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.pickup_table.resizeColumnsToContents()
        self.pickup_table.setSortingEnabled(True)
        self.statusBar().showMessage('Ready.')
        return

    def activate_routes_table(self):
        self.statusBar().showMessage('Refreshing the list of routes. Please wait...')
        sess = classes.create_session(self.engine)
        r = sess.query(classes.Route).all()
        try:
            self.routes_model = tables.SpecialTable(r, parent=self.routes_table)
        except:
            import traceback
            log_result = '\n\n---------- ' + datetime.now().__str__() + ' ----------\n'
            log_result += traceback.format_exc() + '\n' + '-'*48 + '\n'
            with open(r'errorlog.txt', 'ab') as infile:
                infile.write(log_result.encode('utf-8'))
            return
        self.routes_table.setModel(self.routes_model)
        font = QFont("Verdana", 9)
        self.routes_table.setFont(font)
        self.routes_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.routes_table.resizeColumnsToContents()
        self.routes_table.setSortingEnabled(True)
        self.statusBar().showMessage('Ready.')
        return

    def activate_languages(self):
        self.languageComboBox.clear()
        session = classes.create_session(self.engine)
        for i in session.query(classes.Language).all():
            self.languageComboBox.addItem(i.name, userData=i.id)
        classes.kill_session(session)
        self.languageComboBox.setCurrentIndex(-1)

    def activate_vendors_table(self):
        self.statusBar().showMessage('Fetching the list of reps. Please wait...')
        sess = classes.create_session(self.engine)
        r = sess.query(classes.Vendor).all()
        try:
            self.vendors_model = tables.SpecialTable(r, parent=self.vendors_table)
        except:
            import traceback
            log_result = '\n\n---------- ' + datetime.now().__str__() + ' ----------\n'
            log_result += traceback.format_exc() + '\n' + '-'*48 + '\n'
            with open(r'errorlog.txt', 'ab') as infile:
                infile.write(log_result.encode('utf-8'))
            return
        self.vendors_table.setModel(self.vendors_model)
        font = QFont("Verdana", 9)
        self.vendors_table.setFont(font)
        self.vendors_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.vendors_table.resizeColumnsToContents()
        self.vendors_table.setSortingEnabled(True)
        self.statusBar().showMessage('Ready.')
        return

    def activate_drivers_table(self):
        self.statusBar().showMessage('Refreshing the list of drivers. Please wait...')
        sess = classes.create_session(self.engine)
        r = sess.query(classes.Driver).all()
        try:
            self.drivers_model = tables.SpecialTable(r, parent=self.drivers_table)
        except:
            import traceback
            log_result = '\n\n---------- ' + datetime.now().__str__() + ' ----------\n'
            log_result += traceback.format_exc() + '\n' + '-'*48 + '\n'
            with open(r'errorlog.txt', 'ab') as infile:
                infile.write(log_result.encode('utf-8'))
            return
        self.drivers_table.setModel(self.drivers_model)
        font = QFont("Verdana", 9)
        self.drivers_table.setFont(font)
        self.drivers_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.drivers_table.resizeColumnsToContents()
        self.drivers_table.setSortingEnabled(True)
        self.statusBar().showMessage('Ready.')
        return

    def activate_guides_table(self):
        self.statusBar().showMessage('Building the list of guides. Please wait...')
        sess = classes.create_session(self.engine)
        r = sess.query(classes.Guide).all()
        try:
            self.guides_model = tables.SpecialTable(r, parent=self.guides_table)
        except:
            import traceback
            log_result = '\n\n---------- ' + datetime.now().__str__() + ' ----------\n'
            log_result += traceback.format_exc() + '\n' + '-'*48 + '\n'
            with open(r'errorlog.txt', 'ab') as infile:
                infile.write(log_result.encode('utf-8'))
            return
        self.guides_table.setModel(self.guides_model)
        font = QFont("Verdana", 9)
        self.guides_table.setFont(font)
        self.guides_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.guides_table.resizeColumnsToContents()
        self.guides_table.setSortingEnabled(True)
        self.statusBar().showMessage('Ready.')
        return

    def activate_buses_table(self):
        self.statusBar().showMessage('Fetching the list of buses. Please wait...')
        sess = classes.create_session(self.engine)
        r = sess.query(classes.Bus).all()
        try:
            self.buses_model = tables.SpecialTable(r, parent=self.buses_table)
        except:
            import traceback
            log_result = '\n\n---------- ' + datetime.now().__str__() + ' ----------\n'
            log_result += traceback.format_exc() + '\n' + '-'*48 + '\n'
            with open(r'errorlog.txt', 'ab') as infile:
                infile.write(log_result.encode('utf-8'))
            return
        self.buses_table.setModel(self.buses_model)
        font = QFont("Verdana", 9)
        self.buses_table.setFont(font)
        self.buses_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.buses_table.resizeColumnsToContents()
        self.buses_table.setSortingEnabled(True)
        self.statusBar().showMessage('Ready.')
        return

    def activate_trips_table(self, *args):
        self.statusBar().showMessage('Refreshing the list of trips. Please wait...')
        sess = classes.create_session(self.engine)
        fro = self.trips_date_from.date().toPython()
        to = self.trips_date_to.date().toPython()
        d1 = datetime.datetime(fro.year, fro.month, fro.day)
        d2 = datetime.datetime(to.year, to.month, to.day) + datetime.timedelta(days=1)
        r = sess.query(classes.Trip).filter(classes.Trip.start_date_time >= d1,
                                            classes.Trip.start_date_time < d2).all()
        datalist = list()
        for i in r:
            row = list()
            row.append(i.id)
            row.append(i.routes.name)
            row.append(i.start_date_time.strftime("%Y-%m-%d %H:%M"))
            row.append(i.buses.name)
            row.append(i.drivers.name)
            row.append(i.guides.name)
            n = sess.query(classes.Ticket).filter_by(trips_id=i.id).all()
            count = 0
            for j in n:
                count += j.adults
                count += j.children
            row.append(count.__str__()+"/"+i.buses.capacity.__str__())
            datalist.append(row)

        self.trips_model = tables.Trip_Table(datalist)
        self.trips_table.setModel(self.trips_model)
        font = QFont("Verdana", 9)
        self.trips_table.setFont(font)
        self.trips_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.trips_table.resizeColumnsToContents()
        self.trips_table.setSortingEnabled(True)
        self.statusBar().showMessage('Ready.')
        return

    def refresh_utils(self, *args):
        # load = self.loading()
        self.statusBar().showMessage('Retrieving the list of tickets. Please wait...')
        tripid = self.util_combobox_trip.itemData(self.util_combobox_trip.currentIndex())
        if tripid is None:
            self.activate_util_table()
            return
        s = classes.create_session(self.engine)
        tickets = s.query(classes.Ticket).filter_by(trips_id=tripid).all()
        if not tickets:
            classes.kill_session(s)
            msg = QMessageBox()
            msg.setWindowTitle("No tickets found.")
            msg.setText("No tickets to display. Try different search criteria.")
            self.statusBar().showMessage('Ready.')
            msg.exec_()
            return
        datalist = []
        for i in tickets:
            row = []
            row.append(i.id)
            putime = s.query(classes.Pickup_Time).filter_by(routes_id=i.trips.routes.id, pickup_points_id=i.pickup_points.id).first()
            dt = putime.time
            row.append(QTime(i.trips.start_date_time.time()).addSecs(dt*60))
            row.append(i.pickup_point_name)
            row.append(i.hotels.name)
            row.append(i.room)
            row.append(i.customer)
            row.append(i.number)
            row.append(i.adults)
            row.append(i.children)
            row.append(i.infants)
            row.append(i.languages.name) if not i.languages is None else row.append("N/A")
            row.append(i.pay_on_bus)
            row.append(i.vendors.name)
            row.append(i.comments)
            datalist.append(row)
        classes.kill_session(s)
        self.util_ticket_model = tables.Util_Tickets(datalist)
        self.util_passenger_table.setModel(self.util_ticket_model)
        font = QFont("Verdana", 9)
        self.util_passenger_table.setFont(font)
        self.util_passenger_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.util_passenger_table.resizeColumnsToContents()
        self.util_passenger_table.setSortingEnabled(True)
        self.activate_util_table()
        self.statusBar().showMessage('Ready.')

    def activate_util_table(self):
        self.statusBar().showMessage('Refreshing the list of trips on this date. Please wait...')
        tmp = self.util_combobox_trip.currentIndex()
        self.util_combobox_trip.clear()
        sess = classes.create_session(self.engine)
        d = self.util_date_chooser.date().toPython()
        lo = datetime.datetime(d.year, d.month, d.day)
        hi = lo + datetime.timedelta(days=1)
        trips = sess.query(classes.Trip).filter(classes.Trip.start_date_time >= lo,
                                                classes.Trip.start_date_time < hi).all()
        for i in trips:
            self.util_combobox_trip.addItem(i.routes.name + " / " + i.buses.name + " (" + i.buses.capacity.__str__() + ")", userData=i.id)
        if tmp != -1:
            self.util_combobox_trip.setCurrentIndex(tmp)
        else:
            self.util_combobox_trip.setCurrentIndex(0)
        classes.kill_session(sess)
        self.statusBar().showMessage('Ready.')
        #self.refresh_utils(self)

    def activate_fin_table(self):
        self.statusBar().showMessage('Refreshing interface elements. Please wait...')
        self.fin_choose_vendor_combobox.clear()
        self.fin_choose_lang_combobox.clear()
        self.fin_choose_route_combobox.clear()
        sess = classes.create_session(self.engine)
        self.fin_choose_vendor_combobox.addItem("All")
        self.fin_choose_lang_combobox.addItem("All")
        self.fin_choose_route_combobox.addItem("All")
        vendors = sess.query(classes.Vendor).all()
        routes = sess.query(classes.Route).all()
        langs = sess.query(classes.Language).all()
        for i in vendors:
            self.fin_choose_vendor_combobox.addItem(i.name, userData=i.id)
        self.fin_choose_vendor_combobox.setCurrentIndex(0)
        for i in routes:
            self.fin_choose_route_combobox.addItem(i.name, userData=i.id)
        self.fin_choose_route_combobox.setCurrentIndex(0)
        for i in langs:
            self.fin_choose_lang_combobox.addItem(i.name, userData=i.id)
        self.fin_choose_lang_combobox.setCurrentIndex(0)
        classes.kill_session(sess)
        self.statusBar().showMessage('Ready.')
        #self.refresh_fin()

    def refresh_fin(self, *args):
        self.statusBar().showMessage('Refreshing the list of tickets. Please wait...')
        # load = self.loading()
        d1 = self.fin_date_edit_from.date().toPython()
        d2 = self.fin_date_edit_to.date().toPython()
        lo = datetime.datetime(d1.year, d1.month, d1.day)
        hi = datetime.datetime(d2.year, d2.month, d2.day) + datetime.timedelta(days=1)
        s = classes.create_session(self.engine)
        vid = self.fin_choose_vendor_combobox.itemData(self.fin_choose_vendor_combobox.currentIndex())
        lid = self.fin_choose_lang_combobox.itemData(self.fin_choose_lang_combobox.currentIndex())
        rid = self.fin_choose_route_combobox.itemData(self.fin_choose_route_combobox.currentIndex())
        if rid is None:
            subq_trips = s.query(classes.Trip).filter(classes.Trip.start_date_time >= lo,
                                                      classes.Trip.start_date_time <= hi).all()
        else:
            subq_trips = s.query(classes.Trip).filter(classes.Trip.start_date_time >= lo,
                                                      classes.Trip.start_date_time <= hi,
                                                      classes.Trip.routes_id == rid).all()
        subq_trips_id = []
        for i in subq_trips:
            subq_trips_id.append(i.id)
        if not subq_trips_id:
            classes.kill_session(s)
            msg = QMessageBox()
            msg.setWindowTitle("No tickets found.")
            msg.setText("No tickets to display. Try different search criteria.")
            self.statusBar().showMessage('Ready.')
            msg.exec_()
            return
        ticket_query = s.query(classes.Ticket).filter(classes.Ticket.trips_id.in_(subq_trips_id))
        if lid:
            ticket_query = ticket_query.filter_by(languages_id=lid)
        if vid:
            ticket_query = ticket_query.filter_by(vendors_id=vid)
        ticket_list = ticket_query.all()
        #print(ticket_list)
        if not ticket_list:
            classes.kill_session(s)
            msg = QMessageBox()
            msg.setWindowTitle("No tickets found.")
            msg.setText("No tickets to display. Try different search criteria.")
            self.statusBar().showMessage('Ready.')
            msg.exec_()
            return
        datalist = []
        tot = 0.0
        ad = 0
        ch = 0
        infa = 0
        for i in ticket_list:
            row = []
            row.append(i.id)
            row.append(i.vendors.name)
            row.append(QDate(i.trips.start_date_time.date()))
            row.append(i.trips.routes.name)
            row.append(i.number)
            row.append(i.languages.name) if not i.languages is None else row.append("N/A")
            row.append(i.adults)
            row.append(i.children)
            row.append(i.infants)
            row.append(i.price)
            tot += i.price
            ad += i.adults
            ch += i.children
            infa += i.infants
            row.append(i.pay_on_bus)
            datalist.append(row)

        classes.kill_session(s)
        self.fin_label_total.setText("Total: " + tot.__str__())
        self.fin_label_people.setText(ad.__str__() + " AD + " +
                                      ch.__str__() + " CH + " +
                                      infa.__str__() + " IN = " +
                                      (ad + ch + infa).__str__() )
        self.fin_ticket_model = tables.Fin_Tickets(datalist)
        self.fin_ticket_table.setModel(self.fin_ticket_model)
        font = QFont("Verdana", 9)
        self.fin_ticket_table.setFont(font)
        self.fin_ticket_table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.fin_ticket_table.resizeColumnsToContents()
        self.fin_ticket_table.setSortingEnabled(True)
        self.statusBar().showMessage('Ready.')
        # self.finished(load)

    def peopleChanged(self):
        self.statusBar().showMessage('Recalculating price. Please wait...')
        i = self.tripInput.currentIndex()
        if i != -1:
            s = classes.create_session(self.engine)
            trip = s.query(classes.Trip).filter_by(id=self.tripInput.itemData(i)).first()
            self.pricePerCapita = trip.routes.price
            chldrn = self.childrenSpinBox.value()
            adlts = self.adultsSpinBox.value()
            self.totalPrice = adlts * self.pricePerCapita + (1 / 2) * chldrn * self.pricePerCapita
            self.ticketPriceDoubleSpinBox.setValue(self.totalPrice)
            classes.kill_session(s)
        self.statusBar().showMessage('Ready.')

    def payOnBusChanged(self):
        if self.payOnBusCheckBox.isChecked():
            self.payOnBusAmountDoubleSpinBox.setEnabled(True)
        else:
            self.payOnBusAmountDoubleSpinBox.setEnabled(False)

    def not_today_changed(self):
        if self.not_sold_today_chk.isChecked():
            self.sale_date_edit.setEnabled(True)
        else:
            self.sale_date_edit.setEnabled(False)

    def hotelChanged(self):
        self.statusBar().showMessage('Searching for pickup for this hotel. Please wait...')
        p = self.hotelComboBox.itemData(self.hotelComboBox.currentIndex())
        if p is None:
            return
        s = classes.create_session(self.engine)
        hotel = s.query(classes.Hotel).filter_by(id=p).first()
        i = self.pickup_point_combobox.findData(hotel.pickup_points.id)
        self.pickup_point_combobox.setCurrentIndex(i)
        classes.kill_session(s)
        self.pickup_point_changed()

    def pickup_point_changed(self):
        self.statusBar().showMessage('Recalculating pick-up time. Please wait...')
        if self.tripInput.currentIndex() == -1:
            return
        pickup_id = self.pickup_point_combobox.itemData(self.pickup_point_combobox.currentIndex())
        tripindex = self.tripInput.currentIndex()
        trip_id = self.tripInput.itemData(tripindex)
        s = classes.create_session(self.engine)
        pickup = s.query(classes.Pickup_Point).filter_by(id=pickup_id).first()
        trip = s.query(classes.Trip).filter_by(id=trip_id).first()
        route = trip.routes
        time_diff = s.query(classes.Pickup_Time).filter_by(pickup_points=pickup, routes=route).first().time
        qstart_time = QDateTime(trip.start_date_time.date(), trip.start_date_time.time())
        actual_pickup_time = qstart_time.addSecs(time_diff * 60).time()
        self.pickupTimeLabel.setText("Pickup time: " + actual_pickup_time.toString("hh:mm"))
        classes.kill_session(s)
        self.statusBar().showMessage('Ready.')

    def reset_ticket(self):
        self.statusBar().showMessage('Resetting ticket to defaults. Please wait...')
        self.lineedit_client.setText("")
        self.adultsSpinBox.setValue(0)
        self.childrenSpinBox.setValue(0)
        self.infantsSpinBox.setValue(0)

        # Adding languages to the interface
        self.activate_languages()

        # Adding Hotels
        self.hotelCompleterList = []
        self.session = classes.create_session(self.engine)
        for i in self.session.query(classes.Hotel).all():
            self.hotelComboBox.addItem(i.name, userData=i.id)
            self.hotelCompleterList.append(i.name)
        self.hotelCompleter = QCompleter(self.hotelCompleterList, self)
        self.hotelCompleter.setCaseSensitivity(Qt.CaseInsensitive)
        self.hotelComboBox.setCompleter(self.hotelCompleter)
        self.hotelComboBox.setCurrentIndex(-1)

        # Pickups!
        for i in self.session.query(classes.Pickup_Point).all():
            names = json.loads(i.list_names)
            for j in names:
                self.pickup_point_combobox.addItem(i.name + ": " + j, userData=i.id)
            if not names:
                self.pickup_point_combobox.addItem(i.name, userData=i.id)
        self.pickup_point_combobox.setCurrentIndex(-1)

        # Adding Vendors to the interface
        for i in self.session.query(classes.Vendor).all():
            self.chooseVendorComboBox.addItem(i.name, userData=i.id)
        classes.kill_session(self.session)
        self.chooseVendorComboBox.setCurrentIndex(-1)
        self.roomNumberLineEdit.setText("")
        self.commentsTextInput.setText("")
        self.dateOfTheTrip.setDate(QDate.currentDate().addDays(1))
        self.date_changed()
        self.sale_date_edit.setDate(QDate.currentDate())
        self.voucherNumberLineEdit.setText("")
        self.pickupTimeLabel.setText("Pickup time undefined.")
        self.ticketPriceDoubleSpinBox.setValue(0)
        self.payOnBusAmountDoubleSpinBox.setValue(0)
        self.payOnBusCheckBox.setChecked(False)
        self.not_sold_today_chk.setChecked(False)
        self.statusBar().showMessage('Ready.')

    def date_changed(self):
        self.statusBar().showMessage('Refreshing trips for this date. Please wait...')
        d = self.dateOfTheTrip.date().toPython()
        convd = datetime.datetime(d.year, d.month, d.day)
        nextconvd = convd + datetime.timedelta(days=1)
        self.tripInput.clear()
        s = classes.create_session(self.engine)
        avail_trips = s.query(classes.Trip).filter(classes.Trip.start_date_time >= convd,
                                                   classes.Trip.start_date_time < nextconvd).all()
        for i in avail_trips:
            #print(i.routes.name + ": " + i.buses.name)
            self.tripInput.addItem(i.routes.name +
                                   ": " +
                                   i.buses.name +
                                   " (" +
                                   i.buses.capacity.__str__() +
                                   ")", userData=i.id)
        classes.kill_session(s)
        self.tripInput.setCurrentIndex(-1)
        self.routeChanged()

    def databaseOptions(self):
        options = addition_classes.DBOptions()
        result = options.exec_()
        if result == QDialog.Accepted:
            self.dbLoad()

    def change_sig(self):
        w = addition_classes.EditSignature()
        w.exec_()

    def add_new_vendor(self):
        prompt = addition_classes.AddNewVendor(self.engine)
        code = prompt.exec_()
        if code == QDialog.Accepted:
            self.activate_vendors_table()

    def add_new_guide(self):
        prompt = addition_classes.AddNewGuide(self.engine)
        code = prompt.exec_()
        if code == QDialog.Accepted:
            self.activate_guides_table()

    def add_new_driver(self):
        prompt = addition_classes.AddNewDriver(self.engine)
        code = prompt.exec_()
        if code == QDialog.Accepted:
            self.activate_drivers_table()

    def add_new_hotel(self):
        prompt = addition_classes.AddNewHotel(self.engine)
        code = prompt.exec_()
        if code == QDialog.Accepted:
            self.activate_hotels_table()

    def add_new_route(self):
        prompt = addition_classes.AddNewRoute(self.engine)
        code = prompt.exec_()
        if code == QDialog.Accepted:
            self.activate_routes_table()

    def add_new_trip(self):
        prompt = addition_classes.AddNewTrip(self.engine)
        code = prompt.exec_()
        if code == QDialog.Accepted:
            self.activate_trips_table()

    def add_new_bus(self):
        prompt = addition_classes.AddNewBus()
        res = prompt.exec_()
        if res == QDialog.Accepted:
            s = classes.create_session(self.engine)
            s.add(prompt.result)
            classes.kill_session(s)
            self.activate_buses_table()

    def add_pickup_point(self):
        prompt = addition_classes.AddNewPickupPoint(self.engine)
        code = prompt.exec_()
        if code == QDialog.Accepted:
            self.activate_pickuppoints_table()

    def add_language(self):
        prompt = addition_classes.AddNewLanguage(self.engine)
        res = prompt.exec_()
        if res == QDialog.Accepted:
            self.activate_languages()

    # def loading(self):
    #     d = Loading()
    #     d.show()
    #     return d
    #
    # def finished(self, l):
    #     l.hide()

app = QApplication(sys.argv)

splashpic = QPixmap("./Logo.png")
splash = QSplashScreen(splashpic)
splash.show()

dialog = TravManMain()
dialog.show()
splash.finish(dialog)
app.exec_()