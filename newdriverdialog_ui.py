# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\newdriverdialog.ui'
#
# Created: Sat Apr 30 03:18:26 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_new_driver_dialog(object):
    def setupUi(self, new_driver_dialog):
        new_driver_dialog.setObjectName("new_driver_dialog")
        new_driver_dialog.resize(new_driver_dialog.sizeHint().width(), new_driver_dialog.sizeHint().height())
        new_driver_dialog.setModal(True)
        self.formLayout = QtGui.QFormLayout(new_driver_dialog)
        self.formLayout.setObjectName("formLayout")
        self.label_driver_name = QtGui.QLabel(new_driver_dialog)
        self.label_driver_name.setObjectName("label_driver_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_driver_name)
        self.lineedit_driver_name = QtGui.QLineEdit(new_driver_dialog)
        self.lineedit_driver_name.setObjectName("lineedit_driver_name")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lineedit_driver_name)
        self.label_driver_phone = QtGui.QLabel(new_driver_dialog)
        self.label_driver_phone.setObjectName("label_driver_phone")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_driver_phone)
        self.lineedit_driver_phone = QtGui.QLineEdit(new_driver_dialog)
        self.lineedit_driver_phone.setObjectName("lineedit_driver_phone")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.lineedit_driver_phone)
        self.label_driver_comments = QtGui.QLabel(new_driver_dialog)
        self.label_driver_comments.setObjectName("label_driver_comments")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_driver_comments)
        self.lineedit_driver_comments = QtGui.QLineEdit(new_driver_dialog)
        self.lineedit_driver_comments.setObjectName("lineedit_driver_comments")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineedit_driver_comments)
        self.buttonbox_driver = QtGui.QDialogButtonBox(new_driver_dialog)
        self.buttonbox_driver.setOrientation(QtCore.Qt.Horizontal)
        self.buttonbox_driver.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonbox_driver.setObjectName("buttonbox_driver")
        self.formLayout.setWidget(3, QtGui.QFormLayout.SpanningRole, self.buttonbox_driver)

        self.retranslateUi(new_driver_dialog)
        QtCore.QObject.connect(self.buttonbox_driver, QtCore.SIGNAL("accepted()"), new_driver_dialog.accept)
        QtCore.QObject.connect(self.buttonbox_driver, QtCore.SIGNAL("rejected()"), new_driver_dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(new_driver_dialog)

    def retranslateUi(self, new_driver_dialog):
        new_driver_dialog.setWindowTitle(QtGui.QApplication.translate("new_driver_dialog", "Add Driver", None, QtGui.QApplication.UnicodeUTF8))
        self.label_driver_name.setText(QtGui.QApplication.translate("new_driver_dialog", "Name:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_driver_phone.setText(QtGui.QApplication.translate("new_driver_dialog", "Phone:", None, QtGui.QApplication.UnicodeUTF8))
        self.label_driver_comments.setText(QtGui.QApplication.translate("new_driver_dialog", "Comments:", None, QtGui.QApplication.UnicodeUTF8))

